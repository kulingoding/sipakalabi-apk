package com.kulingoding.sipakalabi.data.source.remote.response

import com.google.gson.annotations.SerializedName

data class DataKpp(
    @SerializedName("ESELON")
    var eSELON: String = "",
    @SerializedName("GOLONGAN")
    var gOLONGAN: String = "",
    @SerializedName("JABATAN")
    var jABATAN: String = "",
    @SerializedName("KPP_BERIKUT")
    var kPPBERIKUT: String = "",
    @SerializedName("NAMA")
    var nAMA: String = "",
    @SerializedName("NIP")
    var nIP: String = "",
    @SerializedName("NO_HP")
    var nOHP: String = "",
    @SerializedName("NO_KPP")
    var nOKPP: String = "",
    @SerializedName("NOTIFIKASI")
    var nOTIFIKASI: String = "",
    @SerializedName("TMT_KPP")
    var tMTKPP: String = "",
    @SerializedName("UNIT_KERJA")
    var uNITKERJA: String = "",
    @SerializedName("status")
    var status: String = ""
)