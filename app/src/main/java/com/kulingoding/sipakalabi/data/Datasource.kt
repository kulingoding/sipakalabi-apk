package com.kulingoding.sipakalabi.data

import android.content.Context
import androidx.lifecycle.LiveData
import com.kulingoding.sipakalabi.data.source.remote.response.*
import com.kulingoding.sipakalabi.data.source.remote.response.detailpip.DetailPipResponse

interface Datasource {

    //shared preference
    fun saveNip(nip: String)

    fun saveName(name: String)

    fun saveJabatan(jabatan: String)

    fun saveUnit(unit: String)

    fun getNip(): LiveData<String>?

    fun getName(): LiveData<String>?

    fun deleteSharedPreference()

    fun getJabatan(): LiveData<String>?

    fun getUnit(): LiveData<String>?

    //api
    fun postLogin(nip: String, password: String, context: Context): LiveData<LoginResponse>
    
    fun getPegawai(jabatan: String, unit: String): LiveData<List<PegawaiResponse>>?

    fun getPegawaiPegawai(nip: String, jabatan: String, unit: String): LiveData<List<PegawaiResponse>>?

    fun getGolongan(): LiveData<List<GolonganResponse>>?

    fun getKgb(nip: String, jabatan: String, unit: String): LiveData<KGB>?

    fun getKpp(nip: String, jabatan: String, unit: String): LiveData<KPP>?

    fun getKgbNotif(nip: String, jabatan: String, unit: String): LiveData<KgbResponse>?

    fun getKppNotif(nip: String, jabatan: String, unit: String): LiveData<KppResponse>?

    fun getSppd(jabatan: String, unit: String): LiveData<List<SppdResponse>>?

    fun getSppdPegawai(nip: String, jabatan: String, unit: String): LiveData<List<SppdResponse>>?

    fun getPip(jabatan: String, unit: String): LiveData<List<PipResponse>>?

    fun getPipPegawai(nip: String): LiveData<DetailPipResponse>?

    fun getSearchPegawai(nama: String, jabatan: String, unit: String): LiveData<List<PegawaiResponse>>?

    fun getSearchSppd(nama: String, perjalanan: String, nip: String, jabatan: String, unit: String): LiveData<List<SppdResponse>>?

    fun getPegawaiGolongan(golongan: String): LiveData<List<PegawaiResponse>>?

    fun getSearchPip(nama: String, jabatan: String, unit: String): LiveData<List<PipResponse>>?

    fun getSppdOpd(): LiveData<SppdopdResponse>?

    fun searchSppdOpd(nama: String, perjalanan: String): LiveData<SppdopdResponse>?

}