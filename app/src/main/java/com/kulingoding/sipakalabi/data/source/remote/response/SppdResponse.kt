package com.kulingoding.sipakalabi.data.source.remote.response


import com.google.gson.annotations.SerializedName

data class SppdResponse(
    @SerializedName("Dasar_Surat")
    var dasarSurat: String = "",
    @SerializedName("Jabatan")
    var jabatan: String = "",
    @SerializedName("NIP")
    var nIP: String = "",
    @SerializedName("Nama")
    var nama: String = "",
    @SerializedName("No_SPPD")
    var noSPPD: String = "",
    @SerializedName("No_Surat")
    var noSurat: String = "",
    @SerializedName("Perjalanan")
    var perjalanan: String = "",
    @SerializedName("Selama")
    var selama: String = "",
    @SerializedName("Tanggal")
    var tanggal: String = "",
    @SerializedName("Tgl_brgkt")
    var tglBrgkt: String = "",
    @SerializedName("Tgl_kmbli")
    var tglKmbli: String = "",
    @SerializedName("Tujuan")
    var tujuan: String = "",
    @SerializedName("Unit")
    var unit: String = "",
    @SerializedName("Uraian")
    var uraian: String = ""
)