package com.kulingoding.sipakalabi.data.source.remote.response

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class LoginResponse(
    @SerializedName("nip")
    var nip : String = "",

    @SerializedName("nama_lengkap")
    var namaLengkap: String = "" ,

    @SerializedName("jabatan")
    var jabatan: String = "",

    @SerializedName("unit_kerja")
    var unitKerja: String = ""

):Parcelable