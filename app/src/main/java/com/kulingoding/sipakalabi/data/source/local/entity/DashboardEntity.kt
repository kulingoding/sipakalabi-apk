package com.kulingoding.sipakalabi.data.source.local.entity

import android.graphics.drawable.Drawable

data class DashboardEntity(
    var id: String,
    var title: String,
    var image: Drawable?
)