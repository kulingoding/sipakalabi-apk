package com.kulingoding.sipakalabi.data.source.local

import android.content.Context
import android.content.SharedPreferences

class PreferenceProvider (context: Context){

    private val keyNip = "key_nip"
    private val keyName = "key_name"
    private val keyJabatan = "key_jabatan"
    private val keyUnit = "key_unit"

    private val preference : SharedPreferences = context.getSharedPreferences("user", Context.MODE_PRIVATE)
    private val editor: SharedPreferences.Editor = preference.edit()

    //save function
    private fun saveString(key: String, value: String){
        editor.putString(key, value)
        editor.apply()
    }

    fun saveName(name: String){
        saveString(keyName, name)
    }

    fun saveNip(nip: String){
        saveString(keyNip, nip)

    }

    fun saveJabatan(jabatan: String){
        saveString(keyJabatan, jabatan)
    }

    fun saveUnit(unit: String){
        saveString(keyUnit, unit)
    }

    //get function
    fun getNip(): String?{
        return preference.getString(keyNip, null)
    }

    fun getName(): String?{
        return preference.getString(keyName, null)
    }

    fun getJabatan(): String?{
        return preference.getString(keyJabatan, null)
    }

    fun getUnit(): String?{
        return preference.getString(keyUnit, null)
    }

    //clear/delete function
    fun clearSharedPreference() {

        val editor: SharedPreferences.Editor = preference.edit()
        editor.clear()
        editor.apply()
    }
}