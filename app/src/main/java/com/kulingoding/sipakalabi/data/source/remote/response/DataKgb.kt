package com.kulingoding.sipakalabi.data.source.remote.response


import com.google.gson.annotations.SerializedName

data class DataKgb(
    @SerializedName("ESELON")
    var eSELON: String = "",
    @SerializedName("GOLONGAN")
    var gOLONGAN: String = "",
    @SerializedName("id")
    var id: String = "",
    @SerializedName("JABATAN")
    var jABATAN: String = "",
    @SerializedName("KGB_BERIKUT")
    var kGBBERIKUT: String = "",
    @SerializedName("NAMA")
    var nAMA: String = "",
    @SerializedName("NIP")
    var nIP: String = "",
    @SerializedName("NO_HP")
    var nOHP: String = "",
    @SerializedName("NO_KGB")
    var nOKGB: String = "",
    @SerializedName("NOTIFIKASI")
    var nOTIFIKASI: String = "",
    @SerializedName("TMT_KGB")
    var tMTKGB: String = "",
    @SerializedName("UNIT_KERJA")
    var uNITKERJA: String = ""
)