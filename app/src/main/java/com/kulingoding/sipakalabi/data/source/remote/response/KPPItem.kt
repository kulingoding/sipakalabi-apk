package com.kulingoding.sipakalabi.data.source.remote.response


import com.google.gson.annotations.SerializedName

data class KPPItem(
    @SerializedName("Alamat")
    var alamat: String = "",
    @SerializedName("Eselon")
    var eselon: String = "",
    @SerializedName("Foto")
    var foto: String = "",
    @SerializedName("Golongan")
    var golongan: String = "",
    @SerializedName("Jabatan")
    var jabatan: String = "",
    @SerializedName("Jenis_Kelamin")
    var jenisKelamin: String = "",
    @SerializedName("kpp_berikut")
    var kppBerikut: String = "",
    @SerializedName("Lama_Menduduki_Jabatan")
    var lamaMendudukiJabatan: String = "",
    @SerializedName("NIP")
    var nIP: String = "",
    @SerializedName("Nama_Lengkap")
    var namaLengkap: String = "",
    @SerializedName("No_HP")
    var noHP: String = "",
    @SerializedName("no_kpp")
    var noKpp: String = "",
    @SerializedName("notifikasi")
    var notifikasi: String = "",
    @SerializedName("password")
    var password: String = "",
    @SerializedName("TMT_Jabatan")
    var tMTJabatan: String = "",
    @SerializedName("Tempat_Lahir")
    var tempatLahir: String = "",
    @SerializedName("Tgl_Lahir")
    var tglLahir: String = "",
    @SerializedName("tmt_kpp")
    var tmtKpp: String = "",
    @SerializedName("Unit_kerja")
    var unitKerja: String = ""
)