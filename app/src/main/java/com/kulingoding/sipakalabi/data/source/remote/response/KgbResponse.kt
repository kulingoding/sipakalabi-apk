package com.kulingoding.sipakalabi.data.source.remote.response


import com.google.gson.annotations.SerializedName

data class KgbResponse(
    @SerializedName("data")
    var `data`: List<DataKgb> = listOf(),
    @SerializedName("status")
    var status: String = ""
)