package com.kulingoding.sipakalabi.data.source.remote.response.detailpip


import com.google.gson.annotations.SerializedName

data class DetailPipResponse(
    @SerializedName("data")
    var `data`: Data = Data(),
    @SerializedName("data_sertif")
    var dataSertif: List<DataSertif> = listOf()
)