package com.kulingoding.sipakalabi.data.source.remote

import android.content.Context
import android.widget.Toast
import com.kulingoding.sipakalabi.data.source.remote.response.*

import com.kulingoding.sipakalabi.data.source.remote.network.ApiClient
import com.kulingoding.sipakalabi.data.source.remote.response.detailpip.DetailPipResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class RemoteDatasource{

    private val retrofit = ApiClient()
        .create()

    fun postLogin(
        loginCallback: LoginCallback,
        nip: String,
        password: String,
        context: Context
    ) {
        retrofit.login(nip, password)
            .enqueue(object : Callback<List<LoginResponse>> {
                override fun onFailure(call: Call<List<LoginResponse>>, t: Throwable) {
                    loginCallback.throwable(t)
                }

                override fun onResponse(
                    call: Call<List<LoginResponse>>,
                    response: Response<List<LoginResponse>>
                ) {
                    if (response.isSuccessful) {
                        response.body()?.let {
                            loginCallback.onResponse(response.body()?.get(0))
                        }
                    }else{
                        Toast.makeText(context, "NIP atau Password salah", Toast.LENGTH_SHORT).show()
                    }
                }

            })

    }

    fun getListPegawai(pegawaiCallback: PegawaiCallback, jabatan: String, unit: String) {
        retrofit.getPegawai(jabatan, unit)
            .enqueue(object : Callback<List<PegawaiResponse>>{
                override fun onFailure(call: Call<List<PegawaiResponse>>, t: Throwable) {
                    pegawaiCallback.throwable(t)
                }

                override fun onResponse(
                    call: Call<List<PegawaiResponse>>,
                    response: Response<List<PegawaiResponse>>
                ) {
                    response.body()?.let {
                        pegawaiCallback.onResponse(response.body())
                    }
                }

            })
    }

    fun getListPegawaiPegawai(pegawaiCallback: PegawaiPegawaiCallback, nip: String, jabatan: String, unit: String) {
        retrofit.getPegawaiPegawai(nip, jabatan, unit)
            .enqueue(object : Callback<List<PegawaiResponse>>{
                override fun onFailure(call: Call<List<PegawaiResponse>>, t: Throwable) {
                    pegawaiCallback.throwable(t)
                }

                override fun onResponse(
                    call: Call<List<PegawaiResponse>>,
                    response: Response<List<PegawaiResponse>>
                ) {
                    response.body()?.let {
                        pegawaiCallback.onResponse(response.body())
                    }
                }

            })
    }

    fun getDataKpp(kppCallback: KppCallback, nip: String, jabatan: String, unit: String){
        retrofit.getDataKpp(nip, jabatan, unit)
            .enqueue(object : Callback<KPP>{
                override fun onFailure(call: Call<KPP>, t: Throwable) {
                    kppCallback.throwable(t)
                }

                override fun onResponse(
                    call: Call<KPP>,
                    response: Response<KPP>
                ) {
                    response.body()?.let {
                        kppCallback.onResponse(response.body())
                    }
                }

            })
    }

    fun getDataKgb(kgbCallback: KgbCallback, nip: String, jabatan: String, unit: String){
        retrofit.getDataKgb(nip, jabatan, unit)
            .enqueue(object : Callback<KGB>{
                override fun onFailure(call: Call<KGB>, t: Throwable) {
                    kgbCallback.throwable(t)
                }

                override fun onResponse(
                    call: Call<KGB>,
                    response: Response<KGB>
                ) {
                    response.body()?.let {
                        kgbCallback.onResponse(response.body())
                    }
                }
            })
    }

    fun getDataKppNotif(kppCallback: KppCallbackNotif, nip: String, jabatan: String, unit: String){
        retrofit.getDataKppNotif(nip, jabatan, unit)
            .enqueue(object : Callback<KppResponse>{
                override fun onFailure(call: Call<KppResponse>, t: Throwable) {
                    kppCallback.throwable(t)
                }

                override fun onResponse(
                    call: Call<KppResponse>,
                    response: Response<KppResponse>
                ) {
                    response.body()?.let {
                        kppCallback.onResponse(response.body())
                    }
                }

            })
    }

    fun getDataKgbNotif(kgbCallback: KgbCallbackNotif, nip: String, jabatan: String, unit: String){
        retrofit.getDataKgbNotif(nip, jabatan, unit)
            .enqueue(object : Callback<KgbResponse>{
                override fun onFailure(call: Call<KgbResponse>, t: Throwable) {
                    kgbCallback.throwable(t)
                }

                override fun onResponse(
                    call: Call<KgbResponse>,
                    response: Response<KgbResponse>
                ) {
                    response.body()?.let {
                        kgbCallback.onResponse(response.body())
                    }
                }
            })
    }

    fun getDataSppd(sppdCallback: SppdCallback, jabatan: String, unit: String){
        retrofit.getDataSppd(jabatan, unit)
            .enqueue(object : Callback<List<SppdResponse>>{
                override fun onFailure(call: Call<List<SppdResponse>>, t: Throwable) {
                    sppdCallback.throwable(t)
                }

                override fun onResponse(
                    call: Call<List<SppdResponse>>,
                    response: Response<List<SppdResponse>>
                ) {
                    response.body()?.let {
                        sppdCallback.onResponse(response.body())
                    }
                }
            })
    }

    fun getDataSppdPegawai(sppdCallback: SppdCallback, nip: String, jabatan: String, unit: String){
        retrofit.getDataSppdPegawai(nip, jabatan, unit)
            .enqueue(object : Callback<List<SppdResponse>>{
                override fun onFailure(call: Call<List<SppdResponse>>, t: Throwable) {
                    sppdCallback.throwable(t)
                }

                override fun onResponse(
                    call: Call<List<SppdResponse>>,
                    response: Response<List<SppdResponse>>
                ) {
                    response.body()?.let {
                        sppdCallback.onResponse(response.body())
                    }
                }
            })
    }

    fun getDataGolongan(golonganCallback: GolonganCallback){
        retrofit.getDataGolongan()
            .enqueue(object : Callback<List<GolonganResponse>>{
                override fun onFailure(call: Call<List<GolonganResponse>>, t: Throwable) {
                    golonganCallback.throwable(t)
                }

                override fun onResponse(
                    call: Call<List<GolonganResponse>>,
                    response: Response<List<GolonganResponse>>
                ) {
                    response.body()?.let {
                        golonganCallback.onResponse(response.body())
                    }
                }
            })
    }

    fun getDataPip(pipCallback: PipCallback, jabatan: String, unit: String){
        retrofit.getDataPip(jabatan, unit)
            .enqueue(object : Callback<List<PipResponse>>{
                override fun onFailure(call: Call<List<PipResponse>>, t: Throwable) {
                    pipCallback.throwable(t)
                }

                override fun onResponse(
                    call: Call<List<PipResponse>>,
                    response: Response<List<PipResponse>>
                ) {
                    response.body()?.let {
                        pipCallback.onResponse(response.body())
                    }
                }
            })
    }

    fun getDataPipPegawai(pipCallback: PipPegawaiCallback, nip: String){
        retrofit.getDataPipPegawai(nip)
            .enqueue(object : Callback<DetailPipResponse>{
                override fun onFailure(call: Call<DetailPipResponse>, t: Throwable) {
                    pipCallback.throwable(t)
                }

                override fun onResponse(
                    call: Call<DetailPipResponse>,
                    response: Response<DetailPipResponse>
                ) {
                    response.body()?.let {
                        pipCallback.onResponse(response.body())
                    }
                }
            })
    }

    fun getSearchPegawai(searchPegawai: SearchPegawaiCallback, namma: String, jabatan: String, unit: String){
        retrofit.searchPegawai(namma, jabatan, unit)
            .enqueue(object : Callback<List<PegawaiResponse>>{
                override fun onFailure(call: Call<List<PegawaiResponse>>, t: Throwable) {
                    searchPegawai.throwable(t)
                }

                override fun onResponse(
                    call: Call<List<PegawaiResponse>>,
                    response: Response<List<PegawaiResponse>>
                ) {
                    response.body()?.let {
                        searchPegawai.onResponse(response.body())
                    }
                }
            })
    }

    fun getSearchSppd(searSppdCallback: SearchSppdCallback, nama: String, perjalanan: String, nip: String, jabatan: String, unit: String){
        retrofit.searchSppd(nama, perjalanan, nip, jabatan, unit)
            .enqueue(object : Callback<List<SppdResponse>>{
                override fun onFailure(call: Call<List<SppdResponse>>, t: Throwable) {
                    searSppdCallback.throwable(t)
                }

                override fun onResponse(
                    call: Call<List<SppdResponse>>,
                    response: Response<List<SppdResponse>>
                ) {
                    response.body()?.let {
                        searSppdCallback.onResponse(response.body())
                    }
                }
            })
    }

    fun getListPegawaiGolongan(pegawaiCallback: PegawaiGolonganCallback, golongan: String) {
        retrofit.listPegawaiGolongan(golongan)
            .enqueue(object : Callback<List<PegawaiResponse>>{
                override fun onFailure(call: Call<List<PegawaiResponse>>, t: Throwable) {
                    pegawaiCallback.throwable(t)
                }

                override fun onResponse(
                    call: Call<List<PegawaiResponse>>,
                    response: Response<List<PegawaiResponse>>
                ) {
                    response.body()?.let {
                        pegawaiCallback.onResponse(response.body())
                    }
                }

            })
    }

    fun getSearchPip(searchPipCallback: SearchPipCallback, namma: String, jabatan: String, unit: String){
        retrofit.searchPip(namma, jabatan, unit)
            .enqueue(object : Callback<List<PipResponse>>{
                override fun onFailure(call: Call<List<PipResponse>>, t: Throwable) {
                    searchPipCallback.throwable(t)
                }

                override fun onResponse(
                    call: Call<List<PipResponse>>,
                    response: Response<List<PipResponse>>
                ) {
                    response.body()?.let {
                        searchPipCallback.onResponse(response.body())
                    }
                }
            })
    }

    fun getDataSppdOpd(sppdOpdCallback: SppdOpdCallback){
        retrofit.getDataSppdOpd()
            .enqueue(object : Callback<SppdopdResponse>{
                override fun onFailure(call: Call<SppdopdResponse>, t: Throwable) {
                    sppdOpdCallback.throwable(t)
                }

                override fun onResponse(
                    call: Call<SppdopdResponse>,
                    response: Response<SppdopdResponse>
                ) {
                    response.body()?.let {
                        sppdOpdCallback.onResponse(response.body())
                    }
                }

            })
    }

    fun searchSppdOpd(sppdOpdCallback: SppdOpdCallback, nama: String, perjalanan: String){
        retrofit.searchSppdOpd(nama, perjalanan)
            .enqueue(object : Callback<SppdopdResponse>{
                override fun onFailure(call: Call<SppdopdResponse>, t: Throwable) {
                    sppdOpdCallback.throwable(t)
                }

                override fun onResponse(
                    call: Call<SppdopdResponse>,
                    response: Response<SppdopdResponse>
                ) {
                    response.body()?.let {
                        sppdOpdCallback.onResponse(response.body())
                    }
                }

            })
    }

    interface LoginCallback{
        fun onResponse(login: LoginResponse?)
        fun throwable(t: Throwable)
    }

    interface PegawaiCallback{
        fun onResponse(pegawai: List<PegawaiResponse>?)
        fun throwable(t: Throwable)
    }

    interface PegawaiPegawaiCallback{
        fun onResponse(pegawai: List<PegawaiResponse>?)
        fun throwable(t: Throwable)
    }

    interface KgbCallback{
        fun onResponse(kgb: KGB?)
        fun throwable(t: Throwable)
    }

    interface KppCallback{
        fun onResponse(kpp: KPP?)
        fun throwable(t: Throwable)
    }

    interface KgbCallbackNotif{
        fun onResponse(kgb: KgbResponse?)
        fun throwable(t: Throwable)
    }

    interface KppCallbackNotif{
        fun onResponse(kpp: KppResponse?)
        fun throwable(t: Throwable)
    }

    interface SppdCallback{
        fun onResponse(sppd: List<SppdResponse>?)
        fun throwable(t: Throwable)
    }

    interface GolonganCallback{
        fun onResponse(golongan: List<GolonganResponse>?)
        fun throwable(t: Throwable)
    }

    interface PipCallback{
        fun onResponse(pip: List<PipResponse>?)
        fun throwable(t: Throwable)
    }

    interface PipPegawaiCallback{
        fun onResponse(pip: DetailPipResponse?)
        fun throwable(t: Throwable)
    }

    interface SearchPegawaiCallback{
        fun onResponse(pegawai: List<PegawaiResponse>?)
        fun throwable(t: Throwable)
    }

    interface SearchSppdCallback{
        fun onResponse(sppd: List<SppdResponse>?)
        fun throwable(t: Throwable)
    }

    interface PegawaiGolonganCallback{
        fun onResponse(pegawai: List<PegawaiResponse>?)
        fun throwable(t: Throwable)
    }

    interface SearchPipCallback{
        fun onResponse(pip: List<PipResponse>?)
        fun throwable(t: Throwable)
    }

    interface SppdOpdCallback{
        fun onResponse(sppdOpd: SppdopdResponse?)
        fun throwable(t: Throwable)
    }
}
