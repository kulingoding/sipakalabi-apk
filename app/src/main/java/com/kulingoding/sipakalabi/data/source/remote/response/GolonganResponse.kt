package com.kulingoding.sipakalabi.data.source.remote.response


import com.google.gson.annotations.SerializedName

data class GolonganResponse(
    @SerializedName("golongan")
    var golongan: String = "",
    @SerializedName("total")
    var total: String = ""
)