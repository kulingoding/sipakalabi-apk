package com.kulingoding.sipakalabi.data

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.kulingoding.sipakalabi.data.source.local.PreferenceProvider
import com.kulingoding.sipakalabi.data.source.remote.RemoteDatasource
import com.kulingoding.sipakalabi.data.source.remote.response.*
import com.kulingoding.sipakalabi.data.source.remote.response.detailpip.DetailPipResponse
import timber.log.Timber


class DataRepository (
    private val remoteDatasource: RemoteDatasource,
    private val preferenceProvider: PreferenceProvider
): Datasource{

    override fun postLogin(nip: String, password: String, context: Context): LiveData<LoginResponse> {
        val loginResult = MutableLiveData<LoginResponse>()
        remoteDatasource.postLogin(object : RemoteDatasource.LoginCallback{
            override fun onResponse(login: LoginResponse?) {
                loginResult.postValue(login)
            }

            override fun throwable(t: Throwable) {
                if (t.localizedMessage != null) {
                    Timber.e(t.localizedMessage!!)
                }
            }

        }, nip, password, context)

        return loginResult
    }

    override fun saveNip(nip: String) {
        preferenceProvider.saveNip(nip)
    }

    override fun saveName(name: String) {
        preferenceProvider.saveName(name)
    }

    override fun saveJabatan(jabatan: String) {
        preferenceProvider.saveJabatan(jabatan)
    }

    override fun saveUnit(unit: String) {
        preferenceProvider.saveUnit(unit)
    }

    override fun getNip(): LiveData<String>? {
        val nipResult = MutableLiveData<String>()
        nipResult.postValue(preferenceProvider.getNip())
        return nipResult
    }

    override fun getName(): LiveData<String>? {
        val nameResult = MutableLiveData<String>()
        nameResult.postValue(preferenceProvider.getName())
        return nameResult
    }

    override fun getUnit(): LiveData<String>? {
        val nipResult = MutableLiveData<String>()
        nipResult.postValue(preferenceProvider.getUnit())
        return nipResult
    }

    override fun getJabatan(): LiveData<String>? {
        val jabatan = MutableLiveData<String>()
        jabatan.postValue(preferenceProvider.getJabatan())
        return jabatan
    }

    override fun getPegawai(jabatan: String, unit: String): LiveData<List<PegawaiResponse>>? {
        val pegawaiResult = MutableLiveData<List<PegawaiResponse>>()
        remoteDatasource.getListPegawai(object : RemoteDatasource.PegawaiCallback{
            override fun onResponse(pegawai: List<PegawaiResponse>?) {
                pegawaiResult.postValue(pegawai)
            }

            override fun throwable(t: Throwable) {
                if (t.localizedMessage != null) {
                    Timber.e(t.localizedMessage!!)
                }
            }

        }, jabatan, unit)
        return pegawaiResult
    }

    override fun getPegawaiPegawai(nip: String, jabatan: String, unit: String): LiveData<List<PegawaiResponse>>? {
        val pegawaiResult = MutableLiveData<List<PegawaiResponse>>()
        remoteDatasource.getListPegawaiPegawai(object : RemoteDatasource.PegawaiPegawaiCallback{
            override fun onResponse(pegawai: List<PegawaiResponse>?) {
                pegawaiResult.postValue(pegawai)
            }

            override fun throwable(t: Throwable) {
                if (t.localizedMessage != null) {
                    Timber.e(t.localizedMessage!!)
                }
            }

        }, nip, jabatan, unit)
        return pegawaiResult
    }

    override fun getKgb(nip: String, jabatan: String, unit: String): LiveData<KGB>? {
        val kgbResult = MutableLiveData<KGB>()
        remoteDatasource.getDataKgb(object : RemoteDatasource.KgbCallback{
            override fun onResponse(kgb: KGB?) {
                kgbResult.postValue(kgb)
            }

            override fun throwable(t: Throwable) {
                if (t.localizedMessage != null) {
                    Timber.e(t.localizedMessage!!)
                }
            }

        }, nip, jabatan, unit)
        return kgbResult
    }

    override fun getKpp(nip: String, jabatan: String, unit: String): LiveData<KPP>? {
        val kppResult = MutableLiveData<KPP>()
        remoteDatasource.getDataKpp(object : RemoteDatasource.KppCallback{
            override fun onResponse(kpp: KPP?) {
                kppResult.postValue(kpp)
            }

            override fun throwable(t: Throwable) {
                if (t.localizedMessage != null) {
                    Timber.e(t.localizedMessage!!)
                }
            }

        }, nip, jabatan, unit)
        return kppResult
    }

    override fun getKgbNotif(nip: String, jabatan: String, unit: String): LiveData<KgbResponse>? {
        val kgbResult = MutableLiveData<KgbResponse>()
        remoteDatasource.getDataKgbNotif(object : RemoteDatasource.KgbCallbackNotif{
            override fun onResponse(kgb: KgbResponse?) {
                kgbResult.postValue(kgb)
            }

            override fun throwable(t: Throwable) {
                if (t.localizedMessage != null) {
                    Timber.e(t.localizedMessage!!)
                }
            }

        }, nip, jabatan, unit)
        return kgbResult
    }

    override fun getKppNotif(nip: String, jabatan: String, unit: String): LiveData<KppResponse>? {
        val kppResult = MutableLiveData<KppResponse>()
        remoteDatasource.getDataKppNotif(object : RemoteDatasource.KppCallbackNotif{
            override fun onResponse(kpp: KppResponse?) {
                kppResult.postValue(kpp)
            }

            override fun throwable(t: Throwable) {
                if (t.localizedMessage != null) {
                    Timber.e(t.localizedMessage!!)
                }
            }

        }, nip, jabatan, unit)
        return kppResult
    }

    override fun getSppd(jabatan: String, unit: String): LiveData<List<SppdResponse>>? {
        val sppdResult = MutableLiveData<List<SppdResponse>>()
        remoteDatasource.getDataSppd(object : RemoteDatasource.SppdCallback{
            override fun onResponse(sppd: List<SppdResponse>?) {
                sppdResult.postValue(sppd)
            }

            override fun throwable(t: Throwable) {
                if (t.localizedMessage != null) {
                    Timber.e(t.localizedMessage!!)
                }
            }

        }, jabatan, unit)
        return sppdResult
    }

    override fun getSppdPegawai(nip: String, jabatan: String, unit: String): LiveData<List<SppdResponse>>? {
        val sppdResult = MutableLiveData<List<SppdResponse>>()
        remoteDatasource.getDataSppdPegawai(object : RemoteDatasource.SppdCallback{
            override fun onResponse(sppd: List<SppdResponse>?) {
                sppdResult.postValue(sppd)
            }

            override fun throwable(t: Throwable) {
                if (t.localizedMessage != null) {
                    Timber.e(t.localizedMessage!!)
                }
            }

        }, nip, jabatan, unit)
        return sppdResult
    }

    override fun getGolongan(): LiveData<List<GolonganResponse>>? {
        val golonganResult = MutableLiveData<List<GolonganResponse>>()
        remoteDatasource.getDataGolongan(object : RemoteDatasource.GolonganCallback{
            override fun onResponse(golongan: List<GolonganResponse>?) {
                golonganResult.postValue(golongan)
            }

            override fun throwable(t: Throwable) {
                if (t.localizedMessage != null) {
                    Timber.e(t.localizedMessage!!)
                }
            }
        })
        return golonganResult
    }

    override fun getPip(jabatan: String, unit: String): LiveData<List<PipResponse>>? {
        val pipResult = MutableLiveData<List<PipResponse>>()
        remoteDatasource.getDataPip(object : RemoteDatasource.PipCallback{
            override fun onResponse(pip: List<PipResponse>?) {
                pipResult.postValue(pip)
            }

            override fun throwable(t: Throwable) {
                if (t.localizedMessage != null) {
                    Timber.e(t.localizedMessage!!)
                }
            }
        }, jabatan, unit)
        return pipResult
    }

    override fun getPipPegawai(nip: String): LiveData<DetailPipResponse>? {
        val pipResult = MutableLiveData<DetailPipResponse>()
        remoteDatasource.getDataPipPegawai(object : RemoteDatasource.PipPegawaiCallback{
            override fun onResponse(pip: DetailPipResponse?) {
                pipResult.postValue(pip)
            }

            override fun throwable(t: Throwable) {
                if (t.localizedMessage != null) {
                    Timber.e(t.localizedMessage!!)
                }
            }
        }, nip)
        return pipResult
    }

    override fun getSearchPegawai(nama: String, jabatan: String, unit: String): LiveData<List<PegawaiResponse>>? {
        val pipResult = MutableLiveData<List<PegawaiResponse>>()
        remoteDatasource.getSearchPegawai(object : RemoteDatasource.SearchPegawaiCallback{
            override fun onResponse(pegawai: List<PegawaiResponse>?) {
                pipResult.postValue(pegawai)
            }

            override fun throwable(t: Throwable) {
                if (t.localizedMessage != null) {
                    Timber.e(t.localizedMessage!!)
                }
            }
        }, nama, jabatan, unit)
        return pipResult
    }

    override fun getSearchSppd(nama: String, perjalanan: String, nip: String, jabatan: String, unit: String): LiveData<List<SppdResponse>>? {
        val pipResult = MutableLiveData<List<SppdResponse>>()
        remoteDatasource.getSearchSppd(object : RemoteDatasource.SearchSppdCallback{
            override fun onResponse(sppd: List<SppdResponse>?) {
                pipResult.postValue(sppd)
            }

            override fun throwable(t: Throwable) {
                if (t.localizedMessage != null) {
                    Timber.e(t.localizedMessage!!)
                }
            }
        }, nama, perjalanan, nip, jabatan, unit)
        return pipResult
    }

    override fun getPegawaiGolongan(golongan: String): LiveData<List<PegawaiResponse>>? {
        val pegawaiResult = MutableLiveData<List<PegawaiResponse>>()
        remoteDatasource.getListPegawaiGolongan(object : RemoteDatasource.PegawaiGolonganCallback{
            override fun onResponse(pegawai: List<PegawaiResponse>?) {
                pegawaiResult.postValue(pegawai)
            }

            override fun throwable(t: Throwable) {
                if (t.localizedMessage != null) {
                    Timber.e(t.localizedMessage!!)
                }
            }

        }, golongan)
        return pegawaiResult
    }

    override fun getSearchPip(nama: String, jabatan: String, unit: String): LiveData<List<PipResponse>>? {
        val pipResult = MutableLiveData<List<PipResponse>>()
        remoteDatasource.getSearchPip(object : RemoteDatasource.SearchPipCallback{
            override fun onResponse(pip: List<PipResponse>?) {
                pipResult.postValue(pip)
            }

            override fun throwable(t: Throwable) {
                if (t.localizedMessage != null) {
                    Timber.e(t.localizedMessage!!)
                }
            }
        }, nama, jabatan, unit)
        return pipResult
    }

    override fun getSppdOpd(): LiveData<SppdopdResponse>? {
        val result = MutableLiveData<SppdopdResponse>()
        remoteDatasource.getDataSppdOpd(object : RemoteDatasource.SppdOpdCallback{
            override fun onResponse(sppdOpd: SppdopdResponse?) {
                result.postValue(sppdOpd)
            }

            override fun throwable(t: Throwable) {
                if (t.localizedMessage != null) {
                    Timber.e(t.localizedMessage!!)
                }
            }

        })
        return result
    }

    override fun searchSppdOpd(nama: String, perjalanan: String): LiveData<SppdopdResponse>? {
        val result = MutableLiveData<SppdopdResponse>()
        remoteDatasource.searchSppdOpd(object : RemoteDatasource.SppdOpdCallback{
            override fun onResponse(sppdOpd: SppdopdResponse?) {
                result.postValue(sppdOpd)
            }

            override fun throwable(t: Throwable) {
                if (t.localizedMessage != null) {
                    Timber.e(t.localizedMessage!!)
                }
            }

        }, nama, perjalanan)
        return result
    }

    override fun deleteSharedPreference() {
        preferenceProvider.clearSharedPreference()
    }

}