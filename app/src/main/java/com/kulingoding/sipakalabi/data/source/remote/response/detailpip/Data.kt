package com.kulingoding.sipakalabi.data.source.remote.response.detailpip


import com.google.gson.annotations.SerializedName

data class Data(
    @SerializedName("Disiplin")
    var disiplin: String = "",
    @SerializedName("Esselon")
    var esselon: String = "",
    @SerializedName("Foto")
    var foto: String = "",
    @SerializedName("Golongan")
    var golongan: String = "",
    @SerializedName("id")
    var id: String = "",
    @SerializedName("Integritas")
    var integritas: String = "",
    @SerializedName("Jabatan")
    var jabatan: String = "",
    @SerializedName("Jumlah")
    var jumlah: String = "",
    @SerializedName("Kepemimpinan")
    var kepemimpinan: String = "",
    @SerializedName("Kerjasama")
    var kerjasama: String = "",
    @SerializedName("Keterangan_disiplin")
    var keteranganDisiplin: String = "",
    @SerializedName("Komitmen")
    var komitmen: String = "",
    @SerializedName("Nama")
    var nama: String = "",
    @SerializedName("Nip")
    var nip: String = "",
    @SerializedName("No_Sertifikat_PK")
    var noSertifikatPK: String = "",
    @SerializedName("No_Sertifikat_PS")
    var noSertifikatPS: String = "",
    @SerializedName("No_Sertifikat_PT")
    var noSertifikatPT: String = "",
    @SerializedName("Nomor_surat")
    var nomorSurat: String = "",
    @SerializedName("Orientasi")
    var orientasi: String = "",
    @SerializedName("Pendidikan")
    var pendidikan: String = "",
    @SerializedName("Perihal_disiplin")
    var perihalDisiplin: String = "",
    @SerializedName("Perihal_PK")
    var perihalPK: String = "",
    @SerializedName("Perihal_PS")
    var perihalPS: String = "",
    @SerializedName("Perihal_PT")
    var perihalPT: String = "",
    @SerializedName("Pihak_PK")
    var pihakPK: String = "",
    @SerializedName("Pihak_PS")
    var pihakPS: String = "",
    @SerializedName("Pihak_PT")
    var pihakPT: String = "",
    @SerializedName("Rata_rata")
    var rataRata: String = "",
    @SerializedName("Status_Disiplin")
    var statusDisiplin: String = "",
    @SerializedName("Status_Integritas")
    var statusIntegritas: String = "",
    @SerializedName("Status_Kepemimpinan")
    var statusKepemimpinan: String = "",
    @SerializedName("Status_Kerjasama")
    var statusKerjasama: String = "",
    @SerializedName("Status_Komitmen")
    var statusKomitmen: String = "",
    @SerializedName("Status_Orientasi")
    var statusOrientasi: String = "",
    @SerializedName("Tgl_Kinerja1")
    var tglKinerja1: String = "",
    @SerializedName("Tgl_Kinerja2")
    var tglKinerja2: String = "",
    @SerializedName("Tgl_PK")
    var tglPK: String = "",
    @SerializedName("Tgl_PS")
    var tglPS: String = "",
    @SerializedName("Tgl_PT")
    var tglPT: String = "",
    @SerializedName("Tgl_Surat")
    var tglSurat: String = "",
    @SerializedName("Unit")
    var unit: String = ""
)