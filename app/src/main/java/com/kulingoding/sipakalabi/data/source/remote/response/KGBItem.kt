package com.kulingoding.sipakalabi.data.source.remote.response


import com.google.gson.annotations.SerializedName

data class KGBItem(
    @SerializedName("Alamat")
    var alamat: String = "",
    @SerializedName("Eselon")
    var eselon: String = "",
    @SerializedName("Foto")
    var foto: String = "",
    @SerializedName("Golongan")
    var golongan: String = "",
    @SerializedName("Jabatan")
    var jabatan: String = "",
    @SerializedName("Jenis_Kelamin")
    var jenisKelamin: String = "",
    @SerializedName("kgb_berikut")
    var kgbBerikut: String = "",
    @SerializedName("Lama_Menduduki_Jabatan")
    var lamaMendudukiJabatan: String = "",
    @SerializedName("NIP")
    var nIP: String = "",
    @SerializedName("Nama_Lengkap")
    var namaLengkap: String = "",
    @SerializedName("No_HP")
    var noHP: String = "",
    @SerializedName("no_kgb")
    var noKgb: String = "",
    @SerializedName("notifikasi")
    var notifikasi: String = "",
    @SerializedName("password")
    var password: String = "",
    @SerializedName("TMT_Jabatan")
    var tMTJabatan: String = "",
    @SerializedName("Tempat_Lahir")
    var tempatLahir: String = "",
    @SerializedName("Tgl_Lahir")
    var tglLahir: String = "",
    @SerializedName("tmt_kgb")
    var tmtKgb: String = "",
    @SerializedName("Unit_kerja")
    var unitKerja: String = ""
)