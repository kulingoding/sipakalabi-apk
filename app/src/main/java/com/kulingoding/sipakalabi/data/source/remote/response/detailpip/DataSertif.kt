package com.kulingoding.sipakalabi.data.source.remote.response.detailpip


import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class DataSertif(
    @SerializedName("id")
    var id: String = "",
    @SerializedName("id_pip")
    var idPip: String = "",
    @SerializedName("jenis")
    var jenis: String = "",
    @SerializedName("no_sertif")
    var noSertif: String = "",
    @SerializedName("perihal")
    var perihal: String = "",
    @SerializedName("pihak")
    var pihak: String = "",
    @SerializedName("tgl")
    var tgl: String = ""
): Parcelable