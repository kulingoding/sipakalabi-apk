package com.kulingoding.sipakalabi.data.source.remote.response


import com.google.gson.annotations.SerializedName

data class KppResponse(
    @SerializedName("data")
    var `data`: List<DataKpp> = listOf(),
    @SerializedName("status")
    var status: String = ""
)