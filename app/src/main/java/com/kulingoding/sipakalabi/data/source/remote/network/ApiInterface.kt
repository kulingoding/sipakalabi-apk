package com.kulingoding.sipakalabi.data.source.remote.network

import com.kulingoding.sipakalabi.data.source.remote.response.*
import com.kulingoding.sipakalabi.data.source.remote.response.detailpip.DetailPipResponse
import retrofit2.Call
import retrofit2.http.*

interface ApiInterface {

    @FormUrlEncoded
    @POST("login")
    fun login(
        @Field("nip") nip: String,
        @Field("password") password: String
    ): Call<List<LoginResponse>>

    @GET("pegawai")
    fun getPegawai(
        @Query("jabatan") jabatan: String,
        @Query("unit") unit: String
    ): Call<List<PegawaiResponse>>

    @GET("pegawai")
    fun getPegawaiPegawai(
        @Query("nip")nip: String,
        @Query("jabatan") jabatan: String,
        @Query("unit") unit: String
    ): Call<List<PegawaiResponse>>

    @GET("kpp")
    fun getDataKpp(
        @Query("nip")nip: String,
        @Query("jabatan") jabatan: String,
        @Query("unit") unit: String,
        @Query("request") request: String = "list"
    ): Call<KPP>

    @GET("kgb")
    fun getDataKgb(
        @Query("nip")nip: String,
        @Query("jabatan") jabatan: String,
        @Query("unit") unit: String,
        @Query("request") request: String = "list"
    ): Call<KGB>

    @GET("kpp")
    fun getDataKppNotif(
        @Query("nip")nip: String,
        @Query("jabatan") jabatan: String,
        @Query("unit") unit: String
    ): Call<KppResponse>

    @GET("kgb")
    fun getDataKgbNotif(
        @Query("nip")nip: String,
        @Query("jabatan") jabatan: String,
        @Query("unit") unit: String
    ): Call<KgbResponse>

    @GET("sppd")
    fun getDataSppd(
        @Query("jabatan") jabatan: String,
        @Query("unit") unit: String
    ): Call<List<SppdResponse>>

    @GET("sppd")
    fun getDataSppdPegawai(
        @Query("nip")nip: String,
        @Query("jabatan") jabatan: String,
        @Query("unit") unit: String
    ): Call<List<SppdResponse>>

    @GET("golongan")
    fun getDataGolongan(): Call<List<GolonganResponse>>

    @GET("pip")
    fun getDataPip(
        @Query("jabatan") jabatan: String,
        @Query("unit") unit: String
    ): Call<List<PipResponse>>

    @GET("pip")
    fun getDataPipPegawai(
        @Query("nip")nip: String
    ): Call<DetailPipResponse>

    @FormUrlEncoded
    @POST("pegawai")
    fun searchPegawai(
        @Field("nama")nama: String,
        @Field("jabatan") jabatan: String,
        @Field("unit") unit: String
    ): Call<List<PegawaiResponse>>

    @FormUrlEncoded
    @POST("sppd")
    fun searchSppd(
        @Field("nama")nama: String,
        @Field("perjalanan")perjalanan: String,
        @Field("nip")nip: String,
        @Field("jabatan") jabatan: String,
        @Field("unit") unit: String
    ): Call<List<SppdResponse>>

    @FormUrlEncoded
    @POST("golongan")
    fun listPegawaiGolongan(
        @Field("golongan")golongan: String
    ): Call<List<PegawaiResponse>>

    @FormUrlEncoded
    @POST("pip")
    fun searchPip(
        @Field("nama")nama: String,
        @Field("jabatan") jabatan: String,
        @Field("unit") unit: String
    ): Call<List<PipResponse>>

    @GET("sppdopd")
    fun getDataSppdOpd(): Call<SppdopdResponse>

    @FormUrlEncoded
    @POST("sppdopd")
    fun searchSppdOpd(
        @Field("nama")nama: String,
        @Field("perjalanan") perjalanan: String
    ): Call<SppdopdResponse>
}