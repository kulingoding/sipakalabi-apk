package com.kulingoding.sipakalabi.ui.sppd.search

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.kulingoding.sipakalabi.R
import com.kulingoding.sipakalabi.data.source.remote.response.SppdResponse
import kotlinx.android.synthetic.main.item_sppd.view.*

class SearchSppdAdapter(private val listener: (SppdResponse) -> (Unit)) : RecyclerView.Adapter<SearchSppdAdapter.SearchSppdViewHolder>() {

    private var listSppd = ArrayList<SppdResponse>()

    fun setSppd(sppd: List<SppdResponse>?){
        if (sppd == null) return
        this.listSppd.clear()
        this.listSppd.addAll(sppd)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SearchSppdViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_sppd, parent, false)
        return SearchSppdViewHolder(view)
    }

    override fun getItemCount(): Int = listSppd.size

    override fun onBindViewHolder(holder: SearchSppdViewHolder, position: Int) {
        val menu = listSppd[position]
        holder.bind(menu, listener)
    }

    class SearchSppdViewHolder(view: View): RecyclerView.ViewHolder(view) {

        @SuppressLint("SetTextI18n")
        fun bind(sppd: SppdResponse, listener: (SppdResponse) -> Unit){
            with(itemView){
                tv_name_sppd.text = resources.getString(R.string.name) + sppd.nama
                tv_perjalanan_sppd.text = resources.getString(R.string.perjalanan) + sppd.perjalanan + " ke " + sppd.tujuan
                tv_dasar_perjalanan.text = resources.getString(R.string.dasar_surat) + sppd.dasarSurat
                setOnClickListener {
                    listener(sppd)
                }
            }
        }

    }

}
