package com.kulingoding.sipakalabi.ui.pegawai

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.kulingoding.sipakalabi.data.DataRepository
import com.kulingoding.sipakalabi.data.source.remote.response.PegawaiResponse

class PegawaiViewModel (private val dataRepository: DataRepository): ViewModel() {

    private lateinit var nip : String
    private lateinit var unit : String
    private lateinit var jabatan : String

    fun setNip (nip: String) {
        this.nip = nip
    }

    fun setJabatan(jabatan: String){
        this.jabatan = jabatan
    }

    fun setUnit(unit: String){
        this.unit = unit
    }

    fun geUnit(): LiveData<String>? = dataRepository.getUnit()

    fun getJabatanLocal(): LiveData<String>? = dataRepository.getJabatan()

    fun getNip(): LiveData<String>? = dataRepository.getNip()

    fun getListPegawai() : LiveData<List<PegawaiResponse>>? = dataRepository.getPegawai(jabatan, unit)

    fun getListPegawaiPegawai() : LiveData<List<PegawaiResponse>>? = dataRepository.getPegawaiPegawai(nip, jabatan, unit)
}