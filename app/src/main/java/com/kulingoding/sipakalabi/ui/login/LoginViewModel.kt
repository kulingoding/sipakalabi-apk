package com.kulingoding.sipakalabi.ui.login

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.kulingoding.sipakalabi.data.DataRepository
import com.kulingoding.sipakalabi.data.source.remote.response.LoginResponse

class LoginViewModel (private val dataRepository: DataRepository): ViewModel() {

    private lateinit var nip: String
    private lateinit var password: String
    private lateinit var context: Context

    fun setContext(context: Context){
        this.context = context
    }

    fun loginQuery(nip: String, password: String){
        this.nip = nip
        this.password = password
    }

    fun saveNip(nip: String) = dataRepository.saveNip(nip)

    fun saveJabatan(jabatan: String) = dataRepository.saveJabatan(jabatan)

    fun saveUnit(unit: String) = dataRepository.saveUnit(unit)

    fun saveName(name: String) = dataRepository.saveName(name)

    fun postLogin(): LiveData<LoginResponse> = dataRepository.postLogin(nip, password, context)

    fun getNip(): LiveData<String>? = dataRepository.getNip()
}