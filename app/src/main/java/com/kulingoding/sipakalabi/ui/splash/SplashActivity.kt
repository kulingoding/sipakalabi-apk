package com.kulingoding.sipakalabi.ui.splash

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import com.kulingoding.sipakalabi.R
import com.kulingoding.sipakalabi.ui.login.LoginActivity

class SplashActivity : AppCompatActivity() {

    private val splashScreenTime: Long = 5000

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        Handler().postDelayed({
            val i = Intent(this, LoginActivity::class.java)
            startActivity(i)
            finish()
        }, splashScreenTime)
    }
}
