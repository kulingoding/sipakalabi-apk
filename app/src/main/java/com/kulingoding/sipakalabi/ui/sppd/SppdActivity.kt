package com.kulingoding.sipakalabi.ui.sppd

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.kulingoding.sipakalabi.R
import com.kulingoding.sipakalabi.ui.sppd.search.SearchSppdActivity
import kotlinx.android.synthetic.main.activity_sppd.*
import org.koin.android.viewmodel.ext.android.viewModel
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

class SppdActivity : AppCompatActivity() {

    companion object{
        const val EXTRA_PEGAWAI_STATE = "extra_pegawai_state"
    }

    private val viewModel: SppdViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sppd)

        progress_bar_sppd.visibility = View.VISIBLE

        val extras = intent.extras
        if (extras != null){

            setSupportActionBar(toolbar)
            supportActionBar?.title = "Data SPPD"
            supportActionBar?.setDisplayHomeAsUpEnabled(true)

            val adapterSppd = SppdAdapter{
                val dialog = MaterialAlertDialogBuilder(this, R.style.AlertDialogTheme)
                    .setTitle(it.nama)
                    .setMessage("No Surat : ${it.noSurat}\n" +
                            "No SPPD : ${it.noSPPD}\n" +
                            "Tanggal : ${getDateConvrter(it.tanggal)}\n" +
                            "Nama : ${it.nama}\n" +
                            "NIP : ${it.nIP}\n" +
                            "Jabatan : ${it.jabatan}\n" +
                            "Unit : ${it.unit}\n" +
                            "Tanggal Berangkat : ${getDateConvrter(it.tglBrgkt)}\n" +
                            "Tanggal Kembali : ${getDateConvrter(it.tglKmbli)}\n" +
                            "Selama : ${it.selama} Hari\n" +
                            "Perjawalan : ${it.perjalanan}\n" +
                            "Tujuan : ${it.tujuan}\n" +
                            "Dasar Surat : ${it.dasarSurat}\n" +
                            "Uraian : ${it.uraian}\n" )
                    .setPositiveButton("Ok") { dialogInterface, _ ->
                        dialogInterface.dismiss()
                    }
                dialog.show()
            }

            viewModel.getJabatanLocal()?.observe(this, Observer {
                viewModel.setJabatan(it)
            })

            viewModel.geUnit()?.observe(this, Observer {
                viewModel.setUnit(it)
            })

            viewModel.getNipLocal()?.observe(this, Observer {
                viewModel.setNip(it)
                if (!extras.getBoolean(EXTRA_PEGAWAI_STATE)){
                    viewModel.getDataSppd()?.observe(this, Observer {sppd ->
                        if (sppd.isEmpty()) tv_data_not_found.visibility = View.VISIBLE
                        progress_bar_sppd.visibility = View.GONE
                        adapterSppd.setSppd(sppd)
                        adapterSppd.notifyDataSetChanged()
                    })
                }else{
                    viewModel.getDataSppdPegawai()?.observe(this, Observer { sppd ->
                        if (sppd.isEmpty()) tv_data_not_found.visibility = View.VISIBLE
                        progress_bar_sppd.visibility = View.GONE
                        adapterSppd.setSppd(sppd)
                        adapterSppd.notifyDataSetChanged()
                    })
                }
            })

            with(rv_sppd){
                layoutManager = LinearLayoutManager(this@SppdActivity)
                setHasFixedSize(true)
                adapter = adapterSppd
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_sppd, menu)
        return true
    }
    override fun onOptionsItemSelected(item: MenuItem): Boolean = when (item.itemId) {
        R.id.menu_search_sppd -> {
            val i = Intent(this@SppdActivity, SearchSppdActivity::class.java)
            i.putExtra(SearchSppdActivity.EXTRA_PEGAWAI_STATE, intent.getBooleanExtra(
                EXTRA_PEGAWAI_STATE, false))
            startActivity(i)
            true
        }

        else -> {
            super.onOptionsItemSelected(item)
        }
    }

    @SuppressLint("SimpleDateFormat")
    private fun getDateConvrter(tgl: String?): String? {
        val inputFormat: DateFormat = SimpleDateFormat("yyyy-MM-dd")
        val outputFormat: DateFormat = SimpleDateFormat("dd MMM yyyy")
        val date: Date? = inputFormat.parse(tgl.toString())
        return outputFormat.format(date!!)
    }
}