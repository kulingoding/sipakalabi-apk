package com.kulingoding.sipakalabi.ui.login

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.kulingoding.sipakalabi.R
import com.kulingoding.sipakalabi.ui.home.HomeActivity
import kotlinx.android.synthetic.main.activity_login.*
import org.koin.android.viewmodel.ext.android.viewModel

class LoginActivity : AppCompatActivity() {

    private val viewModel: LoginViewModel by viewModel()
    private lateinit var name : String
    private lateinit var nip : String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        viewModel.setContext(this)

        viewModel.getNip()?.observe(this, Observer {
            if (it != null){
                val i = Intent(this, HomeActivity::class.java)
                startActivity(i)
                finish()
            }
        })

        btn_login.setOnClickListener {
            val nipValue = edt_nip.text.toString()
            val password = edt_password_login.text.toString()

            viewModel.loginQuery(nipValue, password)
            viewModel.postLogin().observe(this, Observer {
                name = it.namaLengkap
                nip = it.nip
                viewModel.saveName(name)
                viewModel.saveNip(nip)
                viewModel.saveJabatan(it.jabatan)
                viewModel.saveUnit(it.unitKerja)

                Log.e("CEKLOGIN", it.unitKerja)
                Log.e("CEKLOGIN", it.jabatan)
                Log.e("CEKLOGIN", it.namaLengkap)
                Log.e("CEKLOGIN", it.nip)
                Toast.makeText(this, "Login Berhasil", Toast.LENGTH_SHORT).show()
                val i = Intent(this, HomeActivity::class.java)
                startActivity(i)
                finish()
            })
        }

    }
}
