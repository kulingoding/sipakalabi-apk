package com.kulingoding.sipakalabi.ui.pip.sertifikat

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.kulingoding.sipakalabi.R
import com.kulingoding.sipakalabi.data.source.remote.response.detailpip.DataSertif
import com.kulingoding.sipakalabi.ui.sertifikat.sertifikat.SertifikatAdapter
import kotlinx.android.synthetic.main.activity_sertifikat.*

class SertifikatActivity : AppCompatActivity() {

    companion object{
        const val EXTRA_LIST_SERTIFIKAT = "extra_list_sertifikat"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sertifikat)

        setSupportActionBar(toolbar)
        supportActionBar?.title = "Sertifikat"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        val extras = intent.extras
        if (extras != null){
            val listSertid = extras.getParcelableArrayList<DataSertif>(EXTRA_LIST_SERTIFIKAT)
            if (listSertid!!.isEmpty()) tv_data_not_found.visibility = View.VISIBLE
            val sertifAdapter = SertifikatAdapter()
            sertifAdapter.setSertifikat(listSertid)
            sertifAdapter.notifyDataSetChanged()

            with(rv_sertif){
                layoutManager = LinearLayoutManager(this@SertifikatActivity)
                setHasFixedSize(true)
                adapter = sertifAdapter
            }

        }else{
            tv_data_not_found.visibility = View.VISIBLE
            Toast.makeText(this, "Data Kosong", Toast.LENGTH_SHORT).show()
        }

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean =
        when (item.itemId){
            android.R.id.home -> {
                this.finish()
                true
            }

            else -> {
                super.onOptionsItemSelected(item)
            }
        }
}