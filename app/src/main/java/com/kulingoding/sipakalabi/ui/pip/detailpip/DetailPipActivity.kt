package com.kulingoding.sipakalabi.ui.pip.detailpip

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.kulingoding.sipakalabi.R
import com.kulingoding.sipakalabi.data.source.remote.response.detailpip.DataSertif
import com.kulingoding.sipakalabi.ui.pip.sertifikat.SertifikatActivity
import com.kulingoding.sipakalabi.ui.sertifikat.sertifikat.SertifikatAdapter
import kotlinx.android.synthetic.main.activity_detail_pip.*
import org.koin.android.viewmodel.ext.android.viewModel
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class DetailPipActivity : AppCompatActivity() {

    private val viewModel: DetailPipViewModel by viewModel()

    companion object{
        const val EXTRA_NIP = "extra_nip"
    }
    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_pip)

        setSupportActionBar(toolbar)
        supportActionBar?.title = "Detail PIP"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        val extras = intent.extras
        if (extras != null){

            viewModel.setNip(extras.getString(EXTRA_NIP).toString())

            val listSertif = ArrayList<DataSertif>()

            viewModel.getDataSppdPegawai()!!.observe(this, androidx.lifecycle.Observer {
                val data = it.data
                tv_nama.text = data.nama
                tv_nip.text = data.nip
                tv_jabatan.text = data.jabatan
                tv_unit_kerja.text = data.unit
                tv_pendidikan.text = data.pendidikan
                tv_periode_tgl_kinerja.text = data.tglKinerja1 + " s/d " + data.tglKinerja2
                tv_orientasi_pelayanan.text = data.orientasi
                tv_status_orientasi_pelayanan.text = data.statusOrientasi
                tv_integritas.text = data.integritas
                tv_status_integritas.text = data.statusIntegritas
                tv_komitmien.text = data.komitmen
                tv_status_komitmien.text = data.statusKomitmen
                tv_disiplin.text = data.disiplin
                tv_status_disiplin.text = data.statusDisiplin
                tv_kerjasama.text = data.kerjasama
                tv_status_kerjasama.text = data.statusKerjasama
                tv_kepemimpinan.text = data.kepemimpinan
                tv_status_kepemimpinan.text = data.statusKepemimpinan
                tv_jumlah.text = data.jumlah
                tv_rata_rata.text = data.rataRata
                tv_no_surat.text = data.nomorSurat
                tv_tanggal_disiplin.text = data.tglSurat
                tv_perihal_disiplin.text = data.perihalDisiplin
                tv_keterangan.text = data.keteranganDisiplin

                if (it.dataSertif.isEmpty()){
                    text_kompetensi.visibility = View.GONE
                }

                listSertif.addAll(it.dataSertif)
                val sertifAdapter = SertifikatAdapter()
                sertifAdapter.setSertifikat(it.dataSertif)
                sertifAdapter.notifyDataSetChanged()

                with(rv_sertif){
                    layoutManager = LinearLayoutManager(this@DetailPipActivity)
                    setHasFixedSize(true)
                    adapter = sertifAdapter
                }
            })

//            btn_lihat_sertif.setOnClickListener {
//                val i = Intent(this, SertifikatActivity::class.java)
//                i.putExtra(SertifikatActivity.EXTRA_LIST_SERTIFIKAT, listSertif)
//                startActivity(i)
//            }
        }
    }

    @SuppressLint("SimpleDateFormat")
    private fun getDateConvrter(tgl: String?): String? {
        val inputFormat: DateFormat = SimpleDateFormat("yyyy-MM-dd")
        val outputFormat: DateFormat = SimpleDateFormat("dd MMM yyyy")
        val date: Date? = inputFormat.parse(tgl.toString())
        return outputFormat.format(date!!)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean =
        when (item.itemId){
            android.R.id.home -> {
                this.finish()
                true
            }

            else -> {
                super.onOptionsItemSelected(item)
            }
        }
}