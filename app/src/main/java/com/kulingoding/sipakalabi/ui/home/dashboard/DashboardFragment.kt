package com.kulingoding.sipakalabi.ui.home.dashboard

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import com.kulingoding.sipakalabi.R
import com.kulingoding.sipakalabi.data.source.local.entity.DashboardEntity
import com.kulingoding.sipakalabi.ui.login.LoginActivity
import com.kulingoding.sipakalabi.utils.NotificationReciver
import kotlinx.android.synthetic.main.fragment_dashboard.*
import org.koin.android.ext.android.inject
import org.koin.android.viewmodel.ext.android.viewModel
import java.util.*

/**
 * A simple [Fragment] subclass.
 */
class DashboardFragment : Fragment() {

    private val notificationReciver: NotificationReciver by inject()

    private lateinit var menu : List<DashboardEntity>

    companion object{
        val TAG = DashboardFragment::class.java.simpleName

        fun newInstance(): DashboardFragment {
            return DashboardFragment()
        }
    }

    private val viewModel: DashboardViewModel by viewModel()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_dashboard, container, false)
    }

    @SuppressLint("SetTextI18n")
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        if (activity!= null){

            viewModel.setContext(activity!!.applicationContext)
            val dashboardAdapter = DashboardAdapter{
                viewModel.deleteSharedPreference()
                val i = Intent(activity, LoginActivity::class.java)
                startActivity(i)
                activity!!.finish()
                notificationReciver.cancelNotification(activity!!.applicationContext, NotificationReciver.TYPE_REPEATING)

            }
            viewModel.getJabatan()?.observe(this, Observer {
                val value = it.toLowerCase(Locale.getDefault())
                if (value == "staf" || value == "staff" || value == "pegawai"){
                    menu = viewModel.getMenuNonAdmin()
                    dashboardAdapter.setPegawaiState(true)
                }else if (value == "sekretaris daerah" || value == "kepala biro umum" || value == "staf ahli gubernur" || value == "asisten i bidang pemerintahan" || value == "asisten ii bidang perekonomian dan pembangunan" || value == "asisten iii administrasi umum"){
                    menu = viewModel.getMenuPlus()
                    dashboardAdapter.setPegawaiState(false)
                }else {
                    menu = viewModel.getMenuAdmin()
                    dashboardAdapter.setPegawaiState(false)
                }
                dashboardAdapter.setMenu(menu)
                dashboardAdapter.notifyDataSetChanged()
            })

            val gridItem = GridLayoutManager(activity, 2)
            gridItem.spanSizeLookup = object : GridLayoutManager.SpanSizeLookup(){
                override fun getSpanSize(position: Int): Int {
                    return if (menu.size % 2 != 0){
                        if (position == menu.size - 1) 2 else 1
                    } else {
                        1
                    }
                }

            }


            with(rv_option_home){
                layoutManager = gridItem
                setHasFixedSize(true)
                adapter = dashboardAdapter
            }

            viewModel.getName()?.observe(this, Observer {
                tv_name_dashboard.text = it
            })

            viewModel.getNip()?.observe(this, Observer {
                tv_nip.text = it
            })
        }
    }

}
