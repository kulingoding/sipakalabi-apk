package com.kulingoding.sipakalabi.ui.home.notification

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.kulingoding.sipakalabi.R
import com.kulingoding.sipakalabi.ui.home.notification.kgb.KgbFragment
import com.kulingoding.sipakalabi.ui.home.notification.kpp.KppFragment

class SectionPagerAdapter (private val context: Context, fm: FragmentManager): FragmentPagerAdapter(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT){

    companion object{
        private val TAB_TITLES = intArrayOf(R.string.kgb , R.string.kpp )
    }

    override fun getItem(position: Int): Fragment =
        when(position){
            0 -> KgbFragment()
            1 -> KppFragment()
            else -> Fragment()
        }

    override fun getPageTitle(position: Int): CharSequence? = context.resources.getString(TAB_TITLES[position])

    override fun getCount(): Int = 2

}