package com.kulingoding.sipakalabi.ui.golongan

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.kulingoding.sipakalabi.R
import kotlinx.android.synthetic.main.activity_golongan.*
import org.koin.android.viewmodel.ext.android.viewModel

class GolonganActivity : AppCompatActivity() {

    companion object{
        const val EXTRA_PEGAWAI_STATE = "extra_pegawai_state"
    }

    private val viewModel: GolonganViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_golongan)

        progress_bar_golongan.visibility = View.VISIBLE
        setSupportActionBar(toolbar)
        supportActionBar?.title = "Data Golongan"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        val golonganAdapter = GolonganAdapter()
        viewModel.getDataGolongan()?.observe(this, Observer {golongan ->
            progress_bar_golongan.visibility = View.GONE
            golonganAdapter.setgolongan(golongan)
            golonganAdapter.notifyDataSetChanged()
        })

        with(rv_golongan){
            layoutManager = LinearLayoutManager(this@GolonganActivity)
            setHasFixedSize(true)
            adapter = golonganAdapter
        }
    }
}