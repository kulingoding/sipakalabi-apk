package com.kulingoding.sipakalabi.ui.pip.detailpip

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.kulingoding.sipakalabi.data.DataRepository
import com.kulingoding.sipakalabi.data.source.remote.response.PipResponse


class DetailPipViewModel (private val dataRepository: DataRepository): ViewModel(){

    private lateinit var nip : String

    fun setNip (nip: String) {
        this.nip = nip
    }

    fun getNip(): LiveData<String>? = dataRepository.getNip()

    fun getDataSppdPegawai() = dataRepository.getPipPegawai(nip)

}