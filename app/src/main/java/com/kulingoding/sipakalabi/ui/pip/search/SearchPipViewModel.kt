package com.kulingoding.sipakalabi.ui.pip.search

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.kulingoding.sipakalabi.data.DataRepository
import com.kulingoding.sipakalabi.data.source.remote.response.PipResponse


class SearchPipViewModel (private val dataRepository: DataRepository): ViewModel(){

    private lateinit var nama : String
    private lateinit var jabatan : String
    private lateinit var unit : String

    fun setNama (nama: String) {
        this.nama = nama
    }

    fun setJabatan(jabatan: String){
        this.jabatan = jabatan
    }

    fun setUnit(unit: String){
        this.unit = unit
    }

    fun geUnit(): LiveData<String>? = dataRepository.getUnit()

    fun getJabatanLocal(): LiveData<String>? = dataRepository.getJabatan()

    fun getSearchPip(): LiveData<List<PipResponse>>? = dataRepository.getSearchPip(nama, jabatan, unit)

}