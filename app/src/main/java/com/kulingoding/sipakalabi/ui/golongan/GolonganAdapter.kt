package com.kulingoding.sipakalabi.ui.golongan

import android.annotation.SuppressLint
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.kulingoding.sipakalabi.R
import com.kulingoding.sipakalabi.data.source.remote.response.GolonganResponse
import com.kulingoding.sipakalabi.ui.golongan.listpegawai.ListPegawaiGolonganActivity
import kotlinx.android.synthetic.main.item_golongan.view.*

class GolonganAdapter : RecyclerView.Adapter<GolonganAdapter.GolonganViewHolder>() {

    private var listGolongan = ArrayList<GolonganResponse>()

    fun setgolongan(golongan: List<GolonganResponse>?){
        if (golongan == null) return
        this.listGolongan.clear()
        this.listGolongan.addAll(golongan)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GolonganViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_golongan, parent, false)
        return GolonganViewHolder(view)
    }

    override fun getItemCount(): Int = listGolongan.size

    override fun onBindViewHolder(holder: GolonganViewHolder, position: Int) {
        val menu = listGolongan[position]
        holder.bind(menu)
    }

    class GolonganViewHolder(view: View): RecyclerView.ViewHolder(view) {

        @SuppressLint("SetTextI18n")
        fun bind(golongan: GolonganResponse){
            with(itemView) {
                tv_golongan.text = resources.getString(R.string.golongan)+golongan.golongan
                tv_total_golongan.text = resources.getString(R.string.total_golongan)+golongan.total

                setOnClickListener {
                    val i = Intent(context, ListPegawaiGolonganActivity::class.java)
                    i.putExtra(ListPegawaiGolonganActivity.EXTRA_GOLONGAN, golongan.golongan)
                    context.startActivity(i)
                }
            }
        }

    }

}
