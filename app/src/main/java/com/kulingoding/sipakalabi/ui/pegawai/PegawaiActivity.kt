package com.kulingoding.sipakalabi.ui.pegawai

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.kulingoding.sipakalabi.R
import com.kulingoding.sipakalabi.ui.pegawai.search.SearchPegawaiActivity
import kotlinx.android.synthetic.main.activity_pegawai.*
import kotlinx.android.synthetic.main.activity_pegawai.toolbar
import org.koin.android.viewmodel.ext.android.viewModel

class PegawaiActivity : AppCompatActivity() {

    companion object{
        const val EXTRA_PEGAWAI_STATE = "extra_pegawai_state"
    }

    private val viewModel: PegawaiViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pegawai)

        val extras = intent.extras
        if (extras != null){

            progress_bar_pegawai.visibility = View.VISIBLE
            setSupportActionBar(toolbar)
            supportActionBar?.title = "Data Pegawai"
            supportActionBar?.setDisplayHomeAsUpEnabled(true)

            val pegawaiAdapter = PegawaiAdapter()

            viewModel.getJabatanLocal()?.observe(this, Observer {
                viewModel.setJabatan(it)
            })

            viewModel.geUnit()?.observe(this, Observer {
                viewModel.setUnit(it)
                Log.e("CEKUNIT", it)
            })

            viewModel.getNip()?.observe(this, Observer {
                viewModel.setNip(it)

                if (!extras.getBoolean(EXTRA_PEGAWAI_STATE)){
                    viewModel.getListPegawai()?.observe(this, Observer {pegawai ->
                        progress_bar_pegawai.visibility = View.GONE
                        pegawaiAdapter.setPegawai(pegawai)
                        pegawaiAdapter.notifyDataSetChanged()
                    })
                } else {
                    viewModel.getListPegawaiPegawai()?.observe(this, Observer {pegawai ->
                        progress_bar_pegawai.visibility = View.GONE
                        pegawaiAdapter.setPegawai(pegawai)
                        pegawaiAdapter.notifyDataSetChanged()
                    })
                }
            })

            with(rv_pegawai){
                layoutManager = LinearLayoutManager(this@PegawaiActivity)
                setHasFixedSize(true)
                adapter = pegawaiAdapter
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val extras = intent.extras
        if (extras != null){
            if (!extras.getBoolean(EXTRA_PEGAWAI_STATE)){
                menuInflater.inflate(R.menu.menu_pegawai, menu)
            }
        }
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean = when (item.itemId) {
        R.id.menu_search_pegawai -> {
            val i = Intent(this@PegawaiActivity, SearchPegawaiActivity::class.java)
            startActivity(i)
            true
        }

        else -> {
            super.onOptionsItemSelected(item)
        }
    }
}
