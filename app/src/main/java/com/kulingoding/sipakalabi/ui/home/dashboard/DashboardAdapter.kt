package com.kulingoding.sipakalabi.ui.home.dashboard

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.kulingoding.sipakalabi.R
import com.kulingoding.sipakalabi.data.source.local.entity.DashboardEntity
import com.kulingoding.sipakalabi.ui.golongan.GolonganActivity
import com.kulingoding.sipakalabi.ui.pegawai.PegawaiActivity
import com.kulingoding.sipakalabi.ui.pip.PipActivity
import com.kulingoding.sipakalabi.ui.sppd.SppdActivity
import com.kulingoding.sipakalabi.ui.sppdopd.SppdOpdActivity
import com.kulingoding.sipakalabi.utils.NotificationReciver
import kotlinx.android.synthetic.main.item_dashboard.view.*

class DashboardAdapter(private val listener: (DashboardEntity) -> (Unit)) : RecyclerView.Adapter<DashboardAdapter.DashboardViewHolder>() {

    private var listMenu = ArrayList<DashboardEntity>()
    private var pegawaiState : Boolean = false

    fun setPegawaiState(pegawaiState: Boolean){
        this.pegawaiState = pegawaiState
    }

    fun setMenu(menu: List<DashboardEntity>?){
        if (menu == null) return
        this.listMenu.clear()
        this.listMenu.addAll(menu)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DashboardViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_dashboard, parent, false)
        return DashboardViewHolder(view)
    }

    override fun getItemCount(): Int = listMenu.size

    override fun onBindViewHolder(holder: DashboardViewHolder, position: Int) {
        val menu = listMenu[position]
        holder.bind(menu, listener, pegawaiState)
    }

    class DashboardViewHolder(view: View): RecyclerView.ViewHolder(view) {

        fun bind(menu: DashboardEntity, listener: (DashboardEntity) -> Unit, pegawai: Boolean){
            with(itemView){
                tv_menu_dashboard.text = menu.title
                iv_menu_dashboard.setImageDrawable(menu.image)

                when (menu.id) {
                    "1" -> {
                        setOnClickListener {
                            val i = Intent(context, PegawaiActivity::class.java)
                            i.putExtra(PegawaiActivity.EXTRA_PEGAWAI_STATE, pegawai)
                            context.startActivity(i)
                        }
                    }
                    "2" -> {
                        setOnClickListener {
                            val i = Intent(context, GolonganActivity::class.java)
                            context.startActivity(i)
                        }
                    }
                    "3" -> {
                        setOnClickListener {
                            val i = Intent(context, SppdActivity::class.java)
                            i.putExtra(SppdActivity.EXTRA_PEGAWAI_STATE, pegawai)
                            context.startActivity(i)
                        }
                    }
                    "4" -> {
                        setOnClickListener {
                            val i = Intent(context, PipActivity::class.java)
                            i.putExtra(PipActivity.EXTRA_PEGAWAI_STATE, pegawai)
                            context.startActivity(i)
                        }
                    }
                    "5" -> {
                        setOnClickListener {
                            listener(menu)
                        }
                    }
                    "6" -> {
                        setOnClickListener {
                            val i = Intent(context, SppdOpdActivity::class.java)
//                            i.putExtra(PipActivity.EXTRA_PEGAWAI_STATE, pegawai)
                            context.startActivity(i)
                        }
                    }
                }
            }
        }

    }

}
