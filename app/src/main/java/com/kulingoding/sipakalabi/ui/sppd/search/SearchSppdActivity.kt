package com.kulingoding.sipakalabi.ui.sppd.search

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.MenuItem
import android.view.View
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.kulingoding.sipakalabi.R
import kotlinx.android.synthetic.main.activity_search_pegawai.toolbar
import kotlinx.android.synthetic.main.activity_search_sppd.*
import org.koin.android.viewmodel.ext.android.viewModel
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

class SearchSppdActivity : AppCompatActivity() {

    companion object{
        const val EXTRA_PEGAWAI_STATE = "extra_pegawai_state"
    }

    private val viewModel: SearchSppdViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search_sppd)

        setSupportActionBar(toolbar)
        supportActionBar?.title = "Cari SPPD"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        val searchSppdAdapter = SearchSppdAdapter{
            val dialog = MaterialAlertDialogBuilder(this@SearchSppdActivity, R.style.AlertDialogTheme)
                .setTitle(it.nama)
                .setMessage("No Surat : ${it.noSurat}\n" +
                        "No SPPD : ${it.noSPPD}\n" +
                        "Tanggal : ${it.tanggal}\n" +
                        "Nama : ${it.nama}\n" +
                        "NIP : ${it.nIP}\n" +
                        "Jabatan : ${it.jabatan}\n" +
                        "Unit : ${it.unit}\n" +
                        "Tanggal Berangkat : ${getDateConvrter(it.tglBrgkt)}\n" +
                        "Tanggal Kembali : ${getDateConvrter(it.tglKmbli)}\n" +
                        "Selama : ${it.selama} Hari\n" +
                        "Perjawalan : ${it.perjalanan}\n" +
                        "Tujuan : ${it.tujuan}\n" +
                        "Dasar Surat : ${it.dasarSurat}\n" +
                        "Uraian : ${it.uraian}\n" )
                .setPositiveButton("Ok") { dialogInterface, _ ->
                    dialogInterface.dismiss()
                }
            dialog.show()
        }

        val extras = intent.extras
        if (extras != null){
            val pegawaiState = extras.getBoolean(EXTRA_PEGAWAI_STATE)
            if (!pegawaiState) inputLayout.visibility = View.VISIBLE

            edt_search_nama.addTextChangedListener(object : TextWatcher{
                override fun afterTextChanged(p0: Editable?) {
                    tv_data_not_found.visibility = View.GONE
                }

                override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                    tv_data_not_found.visibility = View.GONE
                }

                @SuppressLint("SetTextI18n")
                override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                    progress_bar_sppd_search.visibility = View.VISIBLE
                    viewModel.setNama(p0.toString())
                    viewModel.setPerjalanan(edt_search_perjalanan.text.toString())
                    viewModel.getJabatanLocal()?.observe(this@SearchSppdActivity, Observer {
                        viewModel.setJabatan(it)
                    })

                    viewModel.geUnit()?.observe(this@SearchSppdActivity, Observer {unit ->
                        viewModel.setUnit(unit)
                        viewModel.getSearchDataSppdPegawai()?.observe(this@SearchSppdActivity, Observer {sppd ->
                            if (sppd.isEmpty()) tv_data_not_found.visibility = View.VISIBLE
                            progress_bar_sppd_search.visibility = View.GONE
                            tv_total_search.text = "Total Search : ${sppd.size}"
                            searchSppdAdapter.setSppd(sppd)
                            searchSppdAdapter.notifyDataSetChanged()
                        })
                    })

                    with(rv_sppd_search){
                        layoutManager = LinearLayoutManager(this@SearchSppdActivity)
                        setHasFixedSize(true)
                        adapter = searchSppdAdapter
                    }
                }

            })

            edt_search_perjalanan.addTextChangedListener(object : TextWatcher{
                override fun afterTextChanged(p0: Editable?) {
                    tv_data_not_found.visibility = View.GONE
                }

                override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                    tv_data_not_found.visibility = View.GONE
                }

                @SuppressLint("SetTextI18n")
                override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                    if (extras.getBoolean(EXTRA_PEGAWAI_STATE)){
                        viewModel.getNip()?.observe(this@SearchSppdActivity, Observer {nip ->
                            viewModel.setNip(nip)
                            progress_bar_sppd_search.visibility = View.VISIBLE
                            viewModel.setNama(edt_search_nama.text.toString())
                            viewModel.setPerjalanan(p0.toString())
                            viewModel.getJabatanLocal()?.observe(this@SearchSppdActivity, Observer {
                                viewModel.setJabatan(it)
                            })
                            viewModel.geUnit()?.observe(this@SearchSppdActivity, Observer {
                                viewModel.setUnit(it)
                                viewModel.getSearchDataSppdPegawai()?.observe(this@SearchSppdActivity, Observer {sppd->
                                    if (sppd.isEmpty()) tv_data_not_found.visibility = View.VISIBLE
                                    progress_bar_sppd_search.visibility = View.GONE
                                    tv_total_search.text = "Total Search : ${sppd.size}"
                                    searchSppdAdapter.setSppd(sppd)
                                    searchSppdAdapter.notifyDataSetChanged()
                                })
                            })
                        })
                    }else{
                        progress_bar_sppd_search.visibility = View.VISIBLE
                        viewModel.setNama(edt_search_nama.text.toString())
                        viewModel.setPerjalanan(p0.toString())
                        viewModel.getJabatanLocal()?.observe(this@SearchSppdActivity, Observer {
                            viewModel.setJabatan(it)
                        })

                        viewModel.geUnit()?.observe(this@SearchSppdActivity, Observer {unit ->
                            viewModel.setUnit(unit)
                            viewModel.getSearchDataSppdPegawai()?.observe(this@SearchSppdActivity, Observer {
                                if (it.isEmpty()) tv_data_not_found.visibility = View.VISIBLE
                                tv_total_search.text = "Total Search : ${it.size}"
                                progress_bar_sppd_search.visibility = View.GONE
                                searchSppdAdapter.setSppd(it)
                                searchSppdAdapter.notifyDataSetChanged()
                            })
                        })
                    }

                    with(rv_sppd_search){
                        layoutManager = LinearLayoutManager(this@SearchSppdActivity)
                        setHasFixedSize(true)
                        adapter = searchSppdAdapter
                    }
                }

            })
        }

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean =
        when (item.itemId){
            android.R.id.home -> {
                this.finish()
                true
            }

            else -> {
                super.onOptionsItemSelected(item)
            }
        }

    @SuppressLint("SimpleDateFormat")
    private fun getDateConvrter(tgl: String?): String? {
        val inputFormat: DateFormat = SimpleDateFormat("yyyy-MM-dd")
        val outputFormat: DateFormat = SimpleDateFormat("dd MMM yyyy")
        val date: Date? = inputFormat.parse(tgl.toString())
        return outputFormat.format(date!!)
    }
}