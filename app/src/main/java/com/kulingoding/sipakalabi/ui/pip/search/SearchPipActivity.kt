package com.kulingoding.sipakalabi.ui.pip.search

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.MenuItem
import android.view.View
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.kulingoding.sipakalabi.R
import kotlinx.android.synthetic.main.activity_search_pip.*
import kotlinx.android.synthetic.main.activity_search_pip.edt_search
import kotlinx.android.synthetic.main.activity_search_pip.toolbar
import kotlinx.android.synthetic.main.activity_search_pip.tv_data_not_found
import org.koin.android.viewmodel.ext.android.viewModel

class SearchPipActivity : AppCompatActivity() {

    private val viewModel: SearchPipViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search_pip)

        setSupportActionBar(toolbar)
        supportActionBar?.title = "Cari PIP"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)


        edt_search.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                tv_data_not_found.visibility = View.GONE
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                tv_data_not_found.visibility = View.GONE
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                progress_bar_pip_search.visibility = View.VISIBLE
                val searchPipAdapter = SearchPipAdapter()
                if (edt_search.text.toString() == ""){
                    progress_bar_pip_search.visibility = View.GONE
                    edt_search.requestFocus()
                    edt_search.error = "Nama tidak boleh kosong"
                    return
                }
                viewModel.setNama(p0.toString())

                viewModel.getJabatanLocal()?.observe(this@SearchPipActivity, Observer {
                    viewModel.setJabatan(it)
                })

                viewModel.geUnit()?.observe(this@SearchPipActivity, Observer {
                    viewModel.setUnit(it)
                    viewModel.getSearchPip()?.observe(this@SearchPipActivity, Observer {
                        if (it.isEmpty()) tv_data_not_found.visibility = View.VISIBLE
                        progress_bar_pip_search.visibility = View.GONE
                        searchPipAdapter.setPip(it)
                        searchPipAdapter.notifyDataSetChanged()
                    })
                })
                with(rv_pip_search){
                    layoutManager = LinearLayoutManager(this@SearchPipActivity)
                    setHasFixedSize(true)
                    adapter = searchPipAdapter
                }
            }

        })
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean =
        when (item.itemId){
            android.R.id.home -> {
                this.finish()
                true
            }

            else -> {
                super.onOptionsItemSelected(item)
            }
        }
}