package com.kulingoding.sipakalabi.ui.home.notification.kgb

import android.annotation.SuppressLint
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.dialog.MaterialAlertDialogBuilder

import com.kulingoding.sipakalabi.R
import kotlinx.android.synthetic.main.fragment_kgb.*
import kotlinx.android.synthetic.main.fragment_kgb.edt_search_nama
import kotlinx.android.synthetic.main.fragment_kgb.tv_data_not_found
import org.koin.android.viewmodel.ext.android.viewModel
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*


class KgbFragment : Fragment() {

    private val viewModel: KgbViewModel by viewModel()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_kgb, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        progress_bar_kgb.visibility = View.VISIBLE

        viewModel.getJabatanLocal()?.observe(viewLifecycleOwner, Observer {
            viewModel.setJabatan(it)
        })

        viewModel.geUnit()?.observe(viewLifecycleOwner, Observer {
            viewModel.setUnit(it)
        })

        viewModel.getNipLocal()?.observe(this, Observer {
            viewModel.setNip(it)

            val kgbAdapter = KgbAdapter { kgb ->
                val nama = kgb.namaLengkap
                val nip = kgb.nIP
                val jabatan = kgb.jabatan
                val golongan = kgb.golongan
                val eselon = kgb.eselon
                val unitKerja = kgb.unitKerja
                val nokgb = kgb.noKgb
                val tmtKgb = kgb.tmtKgb
                val kgbBerikut = kgb.kgbBerikut
                val notifikasi = kgb.notifikasi
                val noHp = kgb.noHP

                val dialog = MaterialAlertDialogBuilder(requireContext(), R.style.AlertDialogTheme)
                    .setTitle(nama)
                    .setMessage("Nip : $nip\n" +
                            "Jabatan : $jabatan\n" +
                            "Golongan : $golongan\n" +
                            "Eselon : $eselon\n" +
                            "Unit Kerja : $unitKerja\n" +
                            "No KGB Terakhir : $nokgb\n" +
                            "TMT KGB : ${getDateConvrter(tmtKgb)}\n" +
                            "kgb Berikut : ${getDateConvrter(kgbBerikut)}\n" +
                            "Notifikasi : $notifikasi\n" +
                            "No HP : $noHp\n" +
                            "\n\n" +
                            "Untuk segera melampirkan berkas sebagai berikut :\n" +
                            "Surat Pengantar, SK Terakhir, Bebas Temuan.")
                    .setPositiveButton("Ok") { dialogInterface, _ ->
                        dialogInterface.dismiss()
                    }
                dialog.show()
            }
            viewModel.getKgb()?.observe(this, Observer {kgb ->
                progress_bar_kgb.visibility = View.GONE
                if (kgb.isEmpty()){
                    tv_data_not_found.visibility = View.VISIBLE
                } else {
                    kgbAdapter.setkgb(kgb)
                    kgbAdapter.notifyDataSetChanged()
                }

            })

            edt_search_nama.addTextChangedListener(object : TextWatcher {
                override fun afterTextChanged(p0: Editable?) {

                }

                override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

                }

                override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                    kgbAdapter.filter.filter(p0)
                }

            })

            with(rv_kgb){
                layoutManager = LinearLayoutManager(activity)
                setHasFixedSize(true)
                adapter = kgbAdapter
            }
        })

    }

    @SuppressLint("SimpleDateFormat")
    private fun getDateConvrter(tgl: String?): String? {
        val inputFormat: DateFormat = SimpleDateFormat("yyyy-MM-dd")
        val outputFormat: DateFormat = SimpleDateFormat("dd MMM yyyy")
        val date: Date? = inputFormat.parse(tgl.toString())
        return outputFormat.format(date!!)
    }
}
