package com.kulingoding.sipakalabi.ui.pegawai.search

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.MenuItem
import android.view.View
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.kulingoding.sipakalabi.R
import kotlinx.android.synthetic.main.activity_search_pegawai.*
import kotlinx.android.synthetic.main.activity_search_pegawai.toolbar
import org.koin.android.viewmodel.ext.android.viewModel

class SearchPegawaiActivity : AppCompatActivity() {

    private val viewModel: SearchPegawaiViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search_pegawai)

        setSupportActionBar(toolbar)
        supportActionBar?.title = "Cari Pegawai"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        edt_search.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                tv_data_not_found.visibility = View.GONE
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                tv_data_not_found.visibility = View.GONE
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                progress_bar_pegawai_search.visibility = View.VISIBLE
                val searchPegawaiAdapter = SearchPegawaiAdapter()
                if (edt_search.text.toString() == ""){
                    progress_bar_pegawai_search.visibility = View.GONE
                    edt_search.requestFocus()
                    edt_search.error = "Nama tidak boleh kosong"
                    return
                }
                viewModel.setNama(p0.toString())

                viewModel.getJabatanLocal()?.observe(this@SearchPegawaiActivity, Observer {
                    viewModel.setJabatan(it)
                })

                viewModel.geUnit()?.observe(this@SearchPegawaiActivity, Observer {
                    viewModel.setUnit(it)

                    viewModel.getListSearchPegawai()?.observe(this@SearchPegawaiActivity, Observer {
                        if (it.isEmpty()) tv_data_not_found.visibility = View.VISIBLE
                        progress_bar_pegawai_search.visibility = View.GONE
                        searchPegawaiAdapter.setPegawai(it)
                        searchPegawaiAdapter.notifyDataSetChanged()
                    })
                })


                with(rv_pegawai_search){
                    layoutManager = LinearLayoutManager(this@SearchPegawaiActivity)
                    setHasFixedSize(true)
                    adapter = searchPegawaiAdapter
                }
            }

        })

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean =
        when (item.itemId){
            android.R.id.home -> {
                this.finish()
                true
            }

            else -> {
                super.onOptionsItemSelected(item)
            }
         }

}