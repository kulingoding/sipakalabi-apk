package com.kulingoding.sipakalabi.ui.sppdopd

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.kulingoding.sipakalabi.data.DataRepository
import com.kulingoding.sipakalabi.data.source.remote.response.SppdResponse
import com.kulingoding.sipakalabi.data.source.remote.response.SppdopdResponse

class SppdOpdViewModel (private val dataRepository: DataRepository): ViewModel(){

    private lateinit var nama : String
    private lateinit var perjalanan : String

    /*private lateinit var nip : String
    private lateinit var jabatan : String
    private lateinit var unit : String

    fun setNip(nip: String){
        this.nip = nip
    }

    fun setJabatan(jabatan: String){
        this.jabatan = jabatan
    }

    fun setUnit(unit: String){
        this.unit = unit
    }

    fun geUnit(): LiveData<String>? = dataRepository.getUnit()

    fun getJabatanLocal(): LiveData<String>? = dataRepository.getJabatan()

    fun getNipLocal(): LiveData<String>? = dataRepository.getNip()
*/

    fun getName(): LiveData<String>? = dataRepository.getName()

    fun setParameter(nama: String, perjalanan: String){
        this.nama = nama
        this.perjalanan = perjalanan
    }
    fun getDataSppdOpd(): LiveData<SppdopdResponse>? = dataRepository.getSppdOpd()

//    fun getDataSppdPegawai(): LiveData<List<SppdResponse>>? = dataRepository.getSppdPegawai(nip, jabatan, unit)
}