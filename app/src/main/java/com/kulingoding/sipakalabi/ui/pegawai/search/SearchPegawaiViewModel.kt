package com.kulingoding.sipakalabi.ui.pegawai.search

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.kulingoding.sipakalabi.data.DataRepository
import com.kulingoding.sipakalabi.data.source.remote.response.PegawaiResponse

class SearchPegawaiViewModel (private val dataRepository: DataRepository): ViewModel() {

    private lateinit var nama : String
    private lateinit var unit : String
    private lateinit var jabatan : String

    fun setNama (nama: String) {
        this.nama = nama
    }

    fun setJabatan(jabatan: String){
        this.jabatan = jabatan
    }

    fun setUnit(unit: String){
        this.unit = unit
    }

    fun geUnit(): LiveData<String>? = dataRepository.getUnit()

    fun getJabatanLocal(): LiveData<String>? = dataRepository.getJabatan()

    fun getListSearchPegawai() : LiveData<List<PegawaiResponse>>? = dataRepository.getSearchPegawai(nama, jabatan, unit)
}