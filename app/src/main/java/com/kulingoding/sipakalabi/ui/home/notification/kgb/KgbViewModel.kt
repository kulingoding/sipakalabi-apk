package com.kulingoding.sipakalabi.ui.home.notification.kgb

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.kulingoding.sipakalabi.data.DataRepository
import com.kulingoding.sipakalabi.data.source.remote.response.KGB
import com.kulingoding.sipakalabi.data.source.remote.response.KgbResponse

class KgbViewModel (private val dataRepository: DataRepository): ViewModel() {

    private lateinit var nip : String
    private lateinit var jabatan : String
    private lateinit var unit : String

    fun setNip(nip: String){
        this.nip = nip
    }

    fun setJabatan(jabatan: String){
        this.jabatan = jabatan
    }

    fun setUnit(unit: String){
        this.unit = unit
    }

    fun geUnit(): LiveData<String>? = dataRepository.getUnit()

    fun getNipLocal(): LiveData<String>? = dataRepository.getNip()

    fun getJabatanLocal(): LiveData<String>? = dataRepository.getJabatan()

    fun getKgb(): LiveData<KGB>? = dataRepository.getKgb(nip, jabatan, unit)

}