package com.kulingoding.sipakalabi.ui.pegawai

import android.annotation.SuppressLint
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.kulingoding.sipakalabi.R
import com.kulingoding.sipakalabi.data.source.remote.response.PegawaiResponse
import com.kulingoding.sipakalabi.ui.pegawai.detailpegawai.DetailPegawaiActivity
import kotlinx.android.synthetic.main.item_pegawai.view.*

class PegawaiAdapter : RecyclerView.Adapter<PegawaiAdapter.DashboardViewHolder>() {

    private var listPegawai = ArrayList<PegawaiResponse>()

    fun setPegawai(pegawai: List<PegawaiResponse>?){
        if (pegawai == null) return
        this.listPegawai.clear()
        this.listPegawai.addAll(pegawai)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DashboardViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_pegawai, parent, false)
        return DashboardViewHolder(view)
    }

    override fun getItemCount(): Int = listPegawai.size

    override fun onBindViewHolder(holder: DashboardViewHolder, position: Int) {
        val menu = listPegawai[position]
        holder.bind(menu)
    }

    class DashboardViewHolder(view: View): RecyclerView.ViewHolder(view) {

        @SuppressLint("UseCompatLoadingForDrawables")
        fun bind(pegawai: PegawaiResponse){
            with(itemView){
                tv_name_pegawai.text = pegawai.namaLengkap
                tv_unit_kerja_pegawai.text = pegawai.unitKerja
                tv_jabatan_pegawai.text = pegawai.jabatan

//                if (pegawai.jenisKelamin == "Wanita"){
//                    iv_foto_pegawai.setImageDrawable(ContextCompat.getDrawable(context,R.drawable.ic_woman))
//                }else{
//                    iv_foto_pegawai.setImageDrawable(ContextCompat.getDrawable(context,R.drawable.ic_man))
//                }

                //jika di api sudah ada data, hapus kondisi diatas dan pakai fungsi di bawah
                Glide.with(context)
                    .load("https://sipakalaqbi.sulbarprov.go.id/assets/uploads/"+pegawai.foto)
                    .centerCrop()
                    .into(iv_foto_pegawai)

                setOnClickListener {
                    val i = Intent(context, DetailPegawaiActivity::class.java)
                    i.putExtra(DetailPegawaiActivity.EXTRA_ALAMAT, pegawai.alamat)
                    i.putExtra(DetailPegawaiActivity.EXTRA_ESELON, pegawai.eselon)
                    i.putExtra(DetailPegawaiActivity.EXTRA_FOTO, pegawai.foto)
                    i.putExtra(DetailPegawaiActivity.EXTRA_GOLONGAN, pegawai.golongan)
                    i.putExtra(DetailPegawaiActivity.EXTRA_JABATAN, pegawai.jabatan)
                    i.putExtra(DetailPegawaiActivity.EXTRA_JENIS_KELAMIN, pegawai.jenisKelamin)
                    i.putExtra(DetailPegawaiActivity.EXTRA_LAMA_MENJABAT, pegawai.lamaMendudukiJabatan)
                    i.putExtra(DetailPegawaiActivity.EXTRA_NAMA, pegawai.namaLengkap)
                    i.putExtra(DetailPegawaiActivity.EXTRA_NIP, pegawai.nIP)
                    i.putExtra(DetailPegawaiActivity.EXTRA_NO_hp, pegawai.noHP)
                    i.putExtra(DetailPegawaiActivity.EXTRA_TANGGAL_LAHIR, pegawai.tglLahir)
                    i.putExtra(DetailPegawaiActivity.EXTRA_TEMPAT_LAHIR, pegawai.tempatLahir)
                    i.putExtra(DetailPegawaiActivity.EXTRA_TMT_MENJABAT, pegawai.tMTJabatan)
                    i.putExtra(DetailPegawaiActivity.EXTRA_UNIT_KERJA, pegawai.unitKerja)
                    context.startActivity(i)
                }
            }
        }

    }

}
