package com.kulingoding.sipakalabi.ui.home.notification.kpp

import android.annotation.SuppressLint
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.dialog.MaterialAlertDialogBuilder

import com.kulingoding.sipakalabi.R
import kotlinx.android.synthetic.main.fragment_kpp.*
import org.koin.android.viewmodel.ext.android.viewModel
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

/**
 * A simple [Fragment] subclass.
 */
class KppFragment : Fragment() {

    private val viewModel: KppViewModel by viewModel()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_kpp, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        if (activity != null){

            progress_bar_kpp.visibility = View.VISIBLE

            viewModel.getJabatanLocal()?.observe(viewLifecycleOwner, Observer {
                viewModel.setJabatan(it)
            })

            viewModel.geUnit()?.observe(viewLifecycleOwner, Observer {
                viewModel.setUnit(it)
            })

            viewModel.getNipLocal()?.observe(this, Observer {
                viewModel.setNip(it)
                val kppAdapter = KppAdapter{kpp ->
                    val nama = kpp.namaLengkap
                    val nip = kpp.nIP
                    val jabatan = kpp.jabatan
                    val golongan = kpp.golongan
                    val eselon = kpp.eselon
                    val unitKerja = kpp.unitKerja
                    val noKpp = kpp.noKpp
                    val tmtKpp = kpp.tmtKpp
                    val kppBerikut = kpp.kppBerikut
                    val notifikasi = kpp.notifikasi
                    val noHp = kpp.noHP
                    val dialog = MaterialAlertDialogBuilder(requireContext(), R.style.AlertDialogTheme)
                        .setTitle(nama)
                        .setMessage("Nip : $nip\n" +
                                "Jabatan : $jabatan\n" +
                                "Golongan : $golongan\n" +
                                "Eselon : $eselon\n" +
                                "Unit Kerja : $unitKerja\n" +
                                "No KPP Terakhir : $noKpp\n" +
                                "TMT KPP : ${getDateConvrter(tmtKpp)}\n" +
                                "KPP Berikut : ${getDateConvrter(kppBerikut)}\n" +
                                "Notifikasi : $notifikasi\n" +
                                "No HP : $noHp\n" +
                                "\n\n" +
                                "Untuk segera melampirkan berkas sebagai berikut :\n" +
                                "Surat Pengantar, SK Terakhir/Jabatan, Bebas Narkoba, Bebas Temuan, SKP 2 tahun Terakhir.")
                        .setPositiveButton("Ok") { dialogInterface, _ ->
                            dialogInterface.dismiss()
                        }
                    dialog.show()
                }
                viewModel.getKpp()?.observe(this, Observer {kpp ->
                    progress_bar_kpp.visibility = View.GONE
                    if (kpp.isEmpty()){
                        tv_data_not_found.visibility = View.VISIBLE
                    } else {
                        kppAdapter.setKpp(kpp)
                        kppAdapter.notifyDataSetChanged()
                    }

                })

                edt_search_nama.addTextChangedListener(object : TextWatcher{
                    override fun afterTextChanged(p0: Editable?) {

                    }

                    override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

                    }

                    override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                        kppAdapter.filter.filter(p0)
                    }

                })


                with(rv_kpp){
                    layoutManager = LinearLayoutManager(activity)
                    setHasFixedSize(true)
                    adapter = kppAdapter
                }
            })
        }
    }

    @SuppressLint("SimpleDateFormat")
    private fun getDateConvrter(tgl: String?): String? {
        val inputFormat: DateFormat = SimpleDateFormat("yyyy-MM-dd")
        val outputFormat: DateFormat = SimpleDateFormat("dd MMM yyyy")
        val date: Date? = inputFormat.parse(tgl.toString())
        return outputFormat.format(date!!)
    }

}
