package com.kulingoding.sipakalabi.ui.home.notification

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.kulingoding.sipakalabi.R
import kotlinx.android.synthetic.main.fragment_notification.*

class NotificationFragment : Fragment() {

    companion object {
        val TAG = NotificationFragment::class.java.simpleName

        fun newInstance(): NotificationFragment {
            return NotificationFragment()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_notification, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        if (activity != null){
            val sectionPagerAdapter = SectionPagerAdapter(activity!!.applicationContext, childFragmentManager)
            view_pager.adapter = sectionPagerAdapter
            tabs.setupWithViewPager(view_pager)

        }
    }

}
