package com.kulingoding.sipakalabi.ui.golongan

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.kulingoding.sipakalabi.data.DataRepository
import com.kulingoding.sipakalabi.data.source.remote.response.GolonganResponse

class GolonganViewModel (private val dataRepository: DataRepository): ViewModel(){

    fun getDataGolongan(): LiveData<List<GolonganResponse>>? = dataRepository.getGolongan()
}