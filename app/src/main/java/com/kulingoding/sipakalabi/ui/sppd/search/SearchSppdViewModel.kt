package com.kulingoding.sipakalabi.ui.sppd.search

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.kulingoding.sipakalabi.data.DataRepository
import com.kulingoding.sipakalabi.data.source.remote.response.SppdResponse

class SearchSppdViewModel (private val dataRepository: DataRepository): ViewModel(){

    private lateinit var nama : String
    private lateinit var perjalanan : String
    private lateinit var jabatan : String
    private lateinit var unit : String
    private var nip : String = ""

    fun setNip (nip: String) {
        this.nip = nip
    }

    fun setNama (nama: String) {
        this.nama = nama
    }

    fun setPerjalanan(perjalanan: String){
        this.perjalanan = perjalanan
    }

    fun setJabatan(jabatan: String){
        this.jabatan = jabatan
    }

    fun setUnit(unit: String){
        this.unit = unit
    }

    fun geUnit(): LiveData<String>? = dataRepository.getUnit()

    fun getJabatanLocal(): LiveData<String>? = dataRepository.getJabatan()

    fun getNip(): LiveData<String>? = dataRepository.getNip()

    fun getSearchDataSppdPegawai(): LiveData<List<SppdResponse>>? = dataRepository.getSearchSppd(nama, perjalanan, nip, jabatan, unit)
}