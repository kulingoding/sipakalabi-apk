package com.kulingoding.sipakalabi.ui.sppdopd

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.kulingoding.sipakalabi.R
import com.kulingoding.sipakalabi.ui.sppd.SppdActivity
import com.kulingoding.sipakalabi.ui.sppd.search.SearchSppdActivity
import com.kulingoding.sipakalabi.ui.sppdopd.search.SearchSppdOpdActivity
import kotlinx.android.synthetic.main.activity_sppd_opd.*
import kotlinx.android.synthetic.main.activity_sppd_opd.toolbar
import org.koin.android.viewmodel.ext.android.viewModel
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

class SppdOpdActivity : AppCompatActivity() {

    private val viewModel: SppdOpdViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sppd_opd)

        progress_bar_sppdopd.visibility = View.VISIBLE

        setSupportActionBar(toolbar)
        supportActionBar?.title = "Data SPPD Kadis"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        val adapterSppdOpd = SppdOpdAdapter{
            val dialog = MaterialAlertDialogBuilder(this, R.style.AlertDialogTheme)
                .setTitle(it.nama)
                .setMessage("No Surat : ${it.noSurat}\n" +
                        "No SPPD : ${it.noSPPD}\n" +
                        "Tanggal : ${getDateConvrter(it.tanggal)}\n" +
                        "Nama : ${it.nama}\n" +
                        "NIP : ${it.nIP}\n" +
                        "Jabatan : ${it.jabatan}\n" +
                        "Unit : ${it.unit}\n" +
                        "Tanggal Berangkat : ${getDateConvrter(it.tglBrgkt)}\n" +
                        "Tanggal Kembali : ${getDateConvrter(it.tglKmbli)}\n" +
                        "Selama : ${it.selama} Hari\n" +
                        "Perjawalan : ${it.perjalanan}\n" +
                        "Tujuan : ${it.tujuan}\n" +
                        "Dasar Surat : ${it.dasarSurat}\n" +
                        "Uraian : ${it.uraian}\n" )
                .setPositiveButton("Ok") { dialogInterface, _ ->
                    dialogInterface.dismiss()
                }
            dialog.show()
        }

        viewModel.getDataSppdOpd()?.observe(this, Observer {
            progress_bar_sppdopd.visibility = View.GONE
            adapterSppdOpd.setSppd(it)
            adapterSppdOpd.notifyDataSetChanged()
        })

        with(rv_sppdopd){
            layoutManager = LinearLayoutManager(this@SppdOpdActivity)
            setHasFixedSize(true)
            adapter = adapterSppdOpd
        }

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_sppd, menu)
        return true
    }
    override fun onOptionsItemSelected(item: MenuItem): Boolean = when (item.itemId) {
        R.id.menu_search_sppd -> {
            val i = Intent(this@SppdOpdActivity, SearchSppdOpdActivity::class.java)
            i.putExtra(
                SearchSppdActivity.EXTRA_PEGAWAI_STATE, intent.getBooleanExtra(
                    SppdActivity.EXTRA_PEGAWAI_STATE, false))
            startActivity(i)
            true
        }
        android.R.id.home -> {
            this.finish()
            true
        }

        else -> {
            super.onOptionsItemSelected(item)
        }
    }

    @SuppressLint("SimpleDateFormat")
    private fun getDateConvrter(tgl: String?): String? {
        val inputFormat: DateFormat = SimpleDateFormat("yyyy-MM-dd")
        val outputFormat: DateFormat = SimpleDateFormat("dd MMM yyyy")
        val date: Date? = inputFormat.parse(tgl.toString())
        return outputFormat.format(date!!)
    }
}