package com.kulingoding.sipakalabi.ui.home.notification.kpp

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.kulingoding.sipakalabi.data.DataRepository
import com.kulingoding.sipakalabi.data.source.remote.response.KPP
import com.kulingoding.sipakalabi.data.source.remote.response.KppResponse

class KppViewModel (private val dataRepository: DataRepository): ViewModel() {

    private lateinit var nip : String
    private lateinit var jabatan : String
    private lateinit var unit : String

    fun setNip(nip: String){
        this.nip = nip
    }

    fun setJabatan(jabatan: String){
        this.jabatan = jabatan
    }

    fun setUnit(unit: String){
        this.unit = unit
    }

    fun geUnit(): LiveData<String>? = dataRepository.getUnit()

    fun getJabatanLocal(): LiveData<String>? = dataRepository.getJabatan()

    fun getNipLocal(): LiveData<String>? = dataRepository.getNip()

    fun getKpp(): LiveData<KPP>? = dataRepository.getKpp(nip, jabatan, unit)


}