package com.kulingoding.sipakalabi.ui.home.notification.kgb

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import androidx.recyclerview.widget.RecyclerView
import com.kulingoding.sipakalabi.R
import com.kulingoding.sipakalabi.data.source.remote.response.KGBItem
import kotlinx.android.synthetic.main.item_kpp_kgb.view.*
import java.util.*
import kotlin.collections.ArrayList

class KgbAdapter(private val listener:(KGBItem)->Unit) : RecyclerView.Adapter<KgbAdapter.KgbViewHolder>(), Filterable {

    private var listkgb = ArrayList<KGBItem>()
    var kgbFilterList = ArrayList<KGBItem>()

    fun setkgb(kgb: List<KGBItem>?){
        if (kgb == null) return
        this.listkgb.clear()
        this.listkgb.addAll(kgb)
    }

    init {
        kgbFilterList = listkgb
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): KgbViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_kpp_kgb, parent, false)
        return KgbViewHolder(view)
    }

    override fun getItemCount(): Int = kgbFilterList.size

    override fun onBindViewHolder(holder: KgbViewHolder, position: Int) {
        val kgb = kgbFilterList[position]
        holder.bind(kgb, listener)
    }

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(p0: CharSequence?): FilterResults {
                val kppSearch = p0.toString()
                kgbFilterList = if (kppSearch.isEmpty()){
                    listkgb
                }else{
                    val resultList = ArrayList<KGBItem>()
                    for (row in listkgb){
                        if (row.namaLengkap.toLowerCase(Locale.ROOT).contains(kppSearch.toLowerCase(
                                Locale.ROOT))) {
                            resultList.add(row)
                        }
                    }
                    resultList
                }
                val filterResults = FilterResults()
                filterResults.values = kgbFilterList
                return filterResults
            }

            override fun publishResults(p0: CharSequence?, p1: FilterResults?) {
                kgbFilterList = p1?.values as ArrayList<KGBItem>
                notifyDataSetChanged()
            }

        }
    }

    class KgbViewHolder(view: View): RecyclerView.ViewHolder(view) {

        @SuppressLint("SetTextI18n")
        fun bind(kgb: KGBItem, listener: (KGBItem) -> Unit){
            with(itemView){
                tv_name_kpp_kgb.text = resources.getString(R.string.name)+kgb.namaLengkap
                tv_no_kpp_kgb.text = resources.getString(R.string.no_kgb)+kgb.noKgb
                tv_notifikasi_kpp_kgb.text = resources.getString(R.string.notifikasi)+kgb.notifikasi
                setOnClickListener {
                    listener(kgb)
                }
            }
        }

    }

}