package com.kulingoding.sipakalabi.ui.pip

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.kulingoding.sipakalabi.R
import com.kulingoding.sipakalabi.ui.pip.search.SearchPipActivity
import kotlinx.android.synthetic.main.activity_pip.*
import kotlinx.android.synthetic.main.activity_pip.toolbar
import kotlinx.android.synthetic.main.activity_pip.tv_data_not_found
import org.koin.android.viewmodel.ext.android.viewModel

class PipActivity : AppCompatActivity() {

    companion object{
        const val EXTRA_PEGAWAI_STATE = "extra_pegawai_state"
    }

    private val viewModel: PipViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pip)

        val extras = intent.extras
        if (extras != null){
            progress_bar_pip.visibility = View.VISIBLE
            setSupportActionBar(toolbar)
            supportActionBar?.title = "Data PIP"
            supportActionBar?.setDisplayHomeAsUpEnabled(true)

            viewModel.getJabatanLocal()?.observe(this, Observer {
                viewModel.setJabatan(it)
            })

            viewModel.geUnit()?.observe(this, Observer {
                viewModel.setUnit(it)
            })

            val pipAdapter = PipAdapter()
            viewModel.getNip()?.observe(this, Observer {
                viewModel.setNip(it)

               // if (!extras.getBoolean(EXTRA_PEGAWAI_STATE)){
                    viewModel.getDataSppd()?.observe(this, Observer {pip ->
                        if (pip.isEmpty()) tv_data_not_found.visibility = View.VISIBLE
                        progress_bar_pip.visibility = View.GONE
                        pipAdapter.setPip(pip)
                        pipAdapter.notifyDataSetChanged()
                    })
//                } else {
//                    viewModel.getDataSppdPegawai()?.observe(this, Observer {pip ->
//                        if (pip.isEmpty()) tv_data_not_found.visibility = View.VISIBLE
//                        progress_bar_pip.visibility = View.GONE
//                        pipAdapter.setPip(pip)
//                        pipAdapter.notifyDataSetChanged()
//                    })
//                }
            })

            with(rv_pip){
                layoutManager = LinearLayoutManager(this@PipActivity)
                setHasFixedSize(true)
                adapter = pipAdapter
            }
        }

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val extras = intent.extras
        if (extras != null){
            if (!extras.getBoolean(EXTRA_PEGAWAI_STATE)){
                menuInflater.inflate(R.menu.menu_pip, menu)
            }
        }
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean = when (item.itemId) {
        R.id.menu_search_pip -> {
            val i = Intent(this@PipActivity, SearchPipActivity::class.java)
            startActivity(i)
            true
        }

        android.R.id.home -> {
            this.finish()
            true
        }

        else -> {
            super.onOptionsItemSelected(item)
        }
    }
}