package com.kulingoding.sipakalabi.ui.golongan.listpegawai

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.kulingoding.sipakalabi.data.DataRepository
import com.kulingoding.sipakalabi.data.source.remote.response.PegawaiResponse

class ListPegawaiGolonganViewModel (private val dataRepository: DataRepository): ViewModel() {

    private lateinit var golongan : String

    fun setGolongan (golongan: String) {
        this.golongan = golongan
    }

    fun getListPegawaiGolonagn() : LiveData<List<PegawaiResponse>>? = dataRepository.getPegawaiGolongan(golongan)
}