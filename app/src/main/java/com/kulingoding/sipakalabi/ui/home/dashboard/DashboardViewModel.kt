package com.kulingoding.sipakalabi.ui.home.dashboard

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.kulingoding.sipakalabi.data.DataRepository
import com.kulingoding.sipakalabi.data.source.local.entity.DashboardEntity
import com.kulingoding.sipakalabi.utils.DataLocal

class DashboardViewModel (private val dataRepository: DataRepository): ViewModel() {

    private lateinit var context: Context

    fun setContext(context: Context){
        this.context = context
    }

    fun getJabatan(): LiveData<String>? = dataRepository.getJabatan()

    fun getMenuAdmin(): List<DashboardEntity> = DataLocal.getMenuDashboardAdmin(context)

    fun getMenuNonAdmin(): List<DashboardEntity> = DataLocal.getMenuDashboardPegawai(context)

    fun getMenuPlus(): List<DashboardEntity> = DataLocal.getMenuDashboardPlus(context)

    fun getName(): LiveData<String>? = dataRepository.getName()

    fun deleteSharedPreference()= dataRepository.deleteSharedPreference()

    fun getNip(): LiveData<String>? = dataRepository.getNip()

}