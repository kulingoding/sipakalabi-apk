package com.kulingoding.sipakalabi.ui.pegawai.detailpegawai

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.bumptech.glide.Glide
import com.kulingoding.sipakalabi.R
import kotlinx.android.synthetic.main.activity_detail_pegawai.*
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

class DetailPegawaiActivity : AppCompatActivity() {

    companion object{
        const val EXTRA_TEMPAT_LAHIR = "extra_tempat_lahir"
        const val EXTRA_TANGGAL_LAHIR = "extra_tanggal_lahir"
        const val EXTRA_JENIS_KELAMIN = "extra_jenis_kelamin"
        const val EXTRA_ALAMAT = "extra_alamat"
        const val EXTRA_JABATAN = "extra_jabatan"
        const val EXTRA_GOLONGAN = "extra_golongan"
        const val EXTRA_ESELON = "extra_eselon"
        const val EXTRA_UNIT_KERJA = "extra_unit_kerja"
        const val EXTRA_TMT_MENJABAT = "extra_tmt_menjabat"
        const val EXTRA_LAMA_MENJABAT = "extra_lama_menjabat"
        const val EXTRA_NO_hp = "extra_no_hp"
        const val EXTRA_NAMA = "extra_nama"
        const val EXTRA_NIP = "extra_nip"
        const val EXTRA_FOTO = "extra_foto"
    }

    @SuppressLint("UseCompatLoadingForDrawables", "SimpleDateFormat")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_pegawai)

        setSupportActionBar(toolbar)
        supportActionBar?.title = ""
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        val extras = intent.extras
        if (extras != null){


            tv_tgl_lahir_detail_pegawai.text = getDateConvrter(extras.getString(EXTRA_TANGGAL_LAHIR))

            tv_tempat_lahir_detail_pegawai.text = extras.getString(EXTRA_TEMPAT_LAHIR)
            tv_alamat_detail_pegawai.text = extras.getString(EXTRA_ALAMAT)
            tv_eselon_detail_pegawai.text = extras.getString(EXTRA_ESELON)
            tv_golongan_detail_pegawai.text = extras.getString(EXTRA_GOLONGAN)
            tv_jabatan_detail_pegawai.text = extras.getString(EXTRA_JABATAN)
            tv_jk_detail_pegawai.text = extras.getString(EXTRA_JENIS_KELAMIN)
            tv_lama_jabatan_detail_pegawai.text = extras.getString(EXTRA_LAMA_MENJABAT)
            tv_name_detail_pegawai.text = extras.getString(EXTRA_NAMA)
            tv_nip_detail_pegawai.text = extras.getString(EXTRA_NIP)
            tv_no_hp_detail_pegawai.text = extras.getString(EXTRA_NO_hp)
            tv_tmt_jabatan_detail_pegawai.text = getDateConvrter(extras.getString(EXTRA_TMT_MENJABAT))
            tv_unit_kerja_detail_pegawai.text = extras.getString(EXTRA_UNIT_KERJA)


//            if (extras.getString(EXTRA_JENIS_KELAMIN) == "Wanita"){
//                iv_foto_detail_pegawai.setImageDrawable(ContextCompat.getDrawable(this,R.drawable.ic_woman))
//            }else{
//                iv_foto_detail_pegawai.setImageDrawable(ContextCompat.getDrawable(this,R.drawable.ic_man))
//            }
            //jika di api sudah ada data, hapus kondisi diatas dan pakai fungsi di bawah
            Glide.with(this)
                .load("https://sipakalaqbi.sulbarprov.go.id/assets/uploads/"+extras.getString(EXTRA_FOTO))
                .centerCrop()
                .into(iv_foto_detail_pegawai)
        }

        /*btn_kembali.setOnClickListener {
            finish()
        }*/

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean =
        when (item.itemId){
            android.R.id.home -> {
                this.finish()
                true
            }

            else -> {
                super.onOptionsItemSelected(item)
            }
        }

    @SuppressLint("SimpleDateFormat")
    private fun getDateConvrter(tgl: String?): String? {
        val inputFormat: DateFormat = SimpleDateFormat("yyyy-MM-dd")
        val outputFormat: DateFormat = SimpleDateFormat("dd MMM yyyy")
        val date: Date? = inputFormat.parse(tgl.toString())
        return outputFormat.format(date!!)
    }
}
