package com.kulingoding.sipakalabi.ui.sppdopd.search

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.kulingoding.sipakalabi.data.DataRepository
import com.kulingoding.sipakalabi.data.source.remote.response.SppdResponse
import com.kulingoding.sipakalabi.data.source.remote.response.SppdopdResponse

class SearchSppdOpdViewModel (private val dataRepository: DataRepository): ViewModel(){

    private lateinit var nama : String
    private lateinit var perjalanan : String

    fun setParameter(nama: String, perjalanan: String){
        this.nama = nama
        this.perjalanan = perjalanan
    }

    fun getDataSppdOpd(): LiveData<SppdopdResponse>? = dataRepository.searchSppdOpd(nama, perjalanan)

}