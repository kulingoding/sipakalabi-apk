package com.kulingoding.sipakalabi.ui.sertifikat.sertifikat

import android.annotation.SuppressLint
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.kulingoding.sipakalabi.R
import com.kulingoding.sipakalabi.data.source.remote.response.detailpip.DataSertif
import kotlinx.android.synthetic.main.item_sertifikat.view.*
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class SertifikatAdapter : RecyclerView.Adapter<SertifikatAdapter.SertifikatViewHolder>() {

    private var listSertifikat = ArrayList<DataSertif>()

    fun setSertifikat(sertifikat: List<DataSertif>?){
        if (sertifikat == null) return
        this.listSertifikat.clear()
        this.listSertifikat.addAll(sertifikat)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SertifikatViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_sertifikat, parent, false)
        return SertifikatViewHolder(view)
    }

    override fun getItemCount(): Int = listSertifikat.size

    override fun onBindViewHolder(holder: SertifikatViewHolder, position: Int) {
        val menu = listSertifikat[position]
        holder.bind(menu)
    }

    class SertifikatViewHolder(view: View): RecyclerView.ViewHolder(view) {

        @SuppressLint("SetTextI18n")
        fun bind(sertifikat: DataSertif){
            with(itemView){
                tv_jenis_sertif.text =sertifikat.jenis
                tv_no_sertif.text =sertifikat.noSertif
                tv_tanggal_sertifikat.text =getDateConvrter(sertifikat.tgl)
                tv_perihal_sertif.text =sertifikat.perihal
                tv_pihak_sertif.text =sertifikat.pihak

            }
        }

        private fun getDateConvrter(tgl: String?): String? {
            val inputFormat: DateFormat = SimpleDateFormat("yyyy-MM-dd")
            val outputFormat: DateFormat = SimpleDateFormat("dd MMM yyyy")
            val date: Date? = inputFormat.parse(tgl.toString())
            return outputFormat.format(date!!)
        }

    }

}