package com.kulingoding.sipakalabi.ui.sppdopd.search

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.MenuItem
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.kulingoding.sipakalabi.R
import kotlinx.android.synthetic.main.activity_search_sppd_opd.*
import kotlinx.android.synthetic.main.activity_search_sppd_opd.edt_search_nama
import kotlinx.android.synthetic.main.activity_search_sppd_opd.edt_search_perjalanan
import kotlinx.android.synthetic.main.activity_search_sppd_opd.progress_bar_sppd_search
import kotlinx.android.synthetic.main.activity_search_sppd_opd.toolbar
import org.koin.android.viewmodel.ext.android.viewModel
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

class SearchSppdOpdActivity : AppCompatActivity() {

    private val viewModel: SearchSppdOpdViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search_sppd_opd)

        setSupportActionBar(toolbar)
        supportActionBar?.title = "Cari SPPD Kadis"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        val searchSppdAdapter = SearchSppdOpdAdapter{
            val dialog = MaterialAlertDialogBuilder(this@SearchSppdOpdActivity, R.style.AlertDialogTheme)
                .setTitle(it.nama)
                .setMessage("No Surat : ${it.noSurat}\n" +
                        "No SPPD : ${it.noSPPD}\n" +
                        "Tanggal : ${it.tanggal}\n" +
                        "Nama : ${it.nama}\n" +
                        "NIP : ${it.nIP}\n" +
                        "Jabatan : ${it.jabatan}\n" +
                        "Unit : ${it.unit}\n" +
                        "Tanggal Berangkat : ${getDateConvrter(it.tglBrgkt)}\n" +
                        "Tanggal Kembali : ${getDateConvrter(it.tglKmbli)}\n" +
                        "Selama : ${it.selama} Hari\n" +
                        "Perjawalan : ${it.perjalanan}\n" +
                        "Tujuan : ${it.tujuan}\n" +
                        "Dasar Surat : ${it.dasarSurat}\n" +
                        "Uraian : ${it.uraian}\n" )
                .setPositiveButton("Ok") { dialogInterface, _ ->
                    dialogInterface.dismiss()
                }
            dialog.show()
        }

        edt_search_nama.addTextChangedListener(object : TextWatcher{
            override fun afterTextChanged(p0: Editable?) {

            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                viewModel.setParameter(p0.toString(), edt_search_perjalanan.text.toString())
                viewModel.getDataSppdOpd()?.observe(this@SearchSppdOpdActivity, androidx.lifecycle.Observer {
                    if (it.isEmpty()) tv_data_not_found.visibility = View.VISIBLE
                    progress_bar_sppd_search.visibility = View.GONE
                    tv_data_not_found.visibility = View.GONE
                    tv_total_search.text = "Total Search : ${it.size}"
                    searchSppdAdapter.setSppd(it)
                    searchSppdAdapter.notifyDataSetChanged()
                })

                with(rv_sppdopd_search){
                    layoutManager = LinearLayoutManager(this@SearchSppdOpdActivity)
                    setHasFixedSize(true)
                    adapter = searchSppdAdapter
                }
            }

        })

        edt_search_perjalanan.addTextChangedListener(object : TextWatcher{
            override fun afterTextChanged(p0: Editable?) {

            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            @SuppressLint("SetTextI18n")
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                viewModel.setParameter(edt_search_nama.text.toString(), p0.toString())
                viewModel.getDataSppdOpd()?.observe(this@SearchSppdOpdActivity, androidx.lifecycle.Observer {
                    if (it.isEmpty()) tv_data_not_found.visibility = View.VISIBLE
                    progress_bar_sppd_search.visibility = View.GONE
                    tv_data_not_found.visibility = View.GONE
                    tv_total_search.text = "Total Search : ${it.size}"
                    searchSppdAdapter.setSppd(it)
                    searchSppdAdapter.notifyDataSetChanged()
                })

                with(rv_sppdopd_search){
                    layoutManager = LinearLayoutManager(this@SearchSppdOpdActivity)
                    setHasFixedSize(true)
                    adapter = searchSppdAdapter
                }
            }

        })
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean =
        when (item.itemId){
            android.R.id.home -> {
                this.finish()
                true
            }

            else -> {
                super.onOptionsItemSelected(item)
            }
        }

                @SuppressLint("SimpleDateFormat")
    private fun getDateConvrter(tgl: String?): String? {
        val inputFormat: DateFormat = SimpleDateFormat("yyyy-MM-dd")
        val outputFormat: DateFormat = SimpleDateFormat("dd MMM yyyy")
        val date: Date? = inputFormat.parse(tgl.toString())
        return outputFormat.format(date!!)
    }
}