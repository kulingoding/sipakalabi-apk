package com.kulingoding.sipakalabi.ui.home

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.kulingoding.sipakalabi.R
import com.kulingoding.sipakalabi.ui.home.dashboard.DashboardFragment
import com.kulingoding.sipakalabi.ui.home.notification.NotificationFragment
import com.kulingoding.sipakalabi.utils.NotificationReciver
import kotlinx.android.synthetic.main.activity_home.*
import org.koin.android.ext.android.inject

class HomeActivity : AppCompatActivity() {

    private val notificationReciver: NotificationReciver by inject()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        val repeatTime = "09:00"
        val repeatMessage = "Lengkapi berkas KPP/KGB sekarang juga!!"
        val notifId = NotificationReciver.ID_REPEATING

        notificationReciver.getJabatanLocal()?.observe(this, Observer {
            notificationReciver.setJabatan(it)
        })

        notificationReciver.getUnit()?.observe(this, Observer {
            notificationReciver.setUnit(it)
        })

        notificationReciver.getNipLocal()?.observe(this, Observer {
            notificationReciver.setNip(it)

            notificationReciver.setRepeatingNotifiaction(this, NotificationReciver.TYPE_REPEATING, repeatTime, repeatMessage)

            notificationReciver.getKgb()?.observe(this, Observer {kgb ->
                if (kgb.data.isNotEmpty()){
                    Log.e("notif", "ada")
                    notificationReciver.showNotification(this, "Notifikasi KGB", "Lengakapi berkas KGB anda sekarang", notifId)
                }
            })

            notificationReciver.getKpp()?.observe(this, Observer {kpp ->
                Log.e("notif", "ada")
                if (kpp.data.isNotEmpty()){
                    notificationReciver.showNotification(this, "Notifikasi KPP", "Lengakapi berkas KPP anda sekarang", notifId)
                }
            })

        })


        bottom_navigation.setOnNavigationItemSelectedListener {
            when(it.itemId){
                R.id.menu_home -> {
                    val fragment = DashboardFragment
                    replaceFragment(fragment.newInstance(), fragment.TAG)
                }
                R.id.menu_notification -> {
                    val fragment = NotificationFragment
                    replaceFragment(fragment.newInstance(), fragment.TAG)
                }

            }
            true
        }

        bottom_navigation.selectedItemId = R.id.menu_home

    }

    private fun replaceFragment(fragment: Fragment, tag: String){
        supportFragmentManager.beginTransaction()
            .replace(R.id.home_container, fragment, tag)
            .commit()
    }
}
