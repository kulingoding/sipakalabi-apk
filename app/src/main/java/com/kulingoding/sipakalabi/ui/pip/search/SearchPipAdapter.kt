package com.kulingoding.sipakalabi.ui.pip.search

import android.annotation.SuppressLint
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.kulingoding.sipakalabi.R
import com.kulingoding.sipakalabi.data.source.remote.response.PipResponse
import com.kulingoding.sipakalabi.ui.pip.detailpip.DetailPipActivity
import kotlinx.android.synthetic.main.item_pip.view.*

class SearchPipAdapter : RecyclerView.Adapter<SearchPipAdapter.PipViewHolder>() {

    private var listPip = ArrayList<PipResponse>()

    fun setPip(pip: List<PipResponse>?){
        if (pip == null) return
        this.listPip.clear()
        this.listPip.addAll(pip)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PipViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_pip, parent, false)
        return PipViewHolder(view)
    }

    override fun getItemCount(): Int = listPip.size

    override fun onBindViewHolder(holder: PipViewHolder, position: Int) {
        val menu = listPip[position]
        holder.bind(menu)
    }

    class PipViewHolder(view: View): RecyclerView.ViewHolder(view) {

        @SuppressLint("SetTextI18n")
        fun bind(pip: PipResponse){
            with(itemView){
                tv_name_pip.text = resources.getString(R.string.name) + pip.nama
                tv_nip_pip.text = resources.getString(R.string.nip) + pip.nip
                tv_jabatan_pip.text = resources.getString(R.string.jabatan) + pip.jabatan
                tv_unit_kerja_pip.text = resources.getString(R.string.unit_kerja) + pip.unit
                setOnClickListener {
                    val i = Intent(context, DetailPipActivity::class.java)
                    i.putExtra(DetailPipActivity.EXTRA_NIP ,pip.nip)
                    context.startActivity(i)
                }
            }
        }

    }

}