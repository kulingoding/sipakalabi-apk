package com.kulingoding.sipakalabi.ui.sppdopd

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.kulingoding.sipakalabi.R
import com.kulingoding.sipakalabi.data.source.remote.response.SppdopdItem
import kotlinx.android.synthetic.main.item_sppd.view.*

class SppdOpdAdapter(private val listener: (SppdopdItem) -> (Unit)) : RecyclerView.Adapter<SppdOpdAdapter.SppdViewHolder>() {

    private var listSppdOpd = ArrayList<SppdopdItem>()

    fun setSppd(sppd: List<SppdopdItem>?){
        if (sppd == null) return
        this.listSppdOpd.clear()
        this.listSppdOpd.addAll(sppd)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SppdViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_sppd, parent, false)
        return SppdViewHolder(view)
    }

    override fun getItemCount(): Int = listSppdOpd.size

    override fun onBindViewHolder(holder: SppdViewHolder, position: Int) {
        val menu = listSppdOpd[position]
        holder.bind(menu, listener)
    }

    class SppdViewHolder(view: View): RecyclerView.ViewHolder(view) {

        @SuppressLint("SetTextI18n")
        fun bind(sppd: SppdopdItem, listener: (SppdopdItem) -> Unit){
            with(itemView){
                tv_name_sppd.text = resources.getString(R.string.name) + sppd.nama
                tv_perjalanan_sppd.text = resources.getString(R.string.perjalanan) + sppd.perjalanan + " ke " + sppd.tujuan
                tv_dasar_perjalanan.text = resources.getString(R.string.dasar_surat) + sppd.dasarSurat
                setOnClickListener {
                    listener(sppd)
                }
            }
        }

    }

}
