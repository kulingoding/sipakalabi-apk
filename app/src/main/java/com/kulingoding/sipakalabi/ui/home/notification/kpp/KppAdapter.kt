package com.kulingoding.sipakalabi.ui.home.notification.kpp

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import androidx.recyclerview.widget.RecyclerView
import com.kulingoding.sipakalabi.R
import com.kulingoding.sipakalabi.data.source.remote.response.KPPItem
import kotlinx.android.synthetic.main.item_kpp_kgb.view.*
import java.util.*
import kotlin.collections.ArrayList

class KppAdapter(private val listener:(KPPItem)->Unit) : RecyclerView.Adapter<KppAdapter.KppViewHolder>(), Filterable {

    private var listKpp = ArrayList<KPPItem>()

    var kppFilterList = ArrayList<KPPItem>()

    fun setKpp(Kpp: List<KPPItem>?){
        if (Kpp == null) return
        this.listKpp.clear()
        this.listKpp.addAll(Kpp)
    }

    init {
        kppFilterList = listKpp
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): KppViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_kpp_kgb, parent, false)
        return KppViewHolder(view)
    }

    override fun getItemCount(): Int = kppFilterList.size

    override fun onBindViewHolder(holder: KppViewHolder, position: Int) {
        val kpp = kppFilterList[position]
        holder.bind(kpp, listener)
    }

    class KppViewHolder(view: View): RecyclerView.ViewHolder(view) {

        @SuppressLint("SetTextI18n")
        fun bind(kpp: KPPItem, listener:(KPPItem)->Unit){
            with(itemView){
                tv_name_kpp_kgb.text = resources.getString(R.string.name)+kpp.namaLengkap
                tv_no_kpp_kgb.text = resources.getString(R.string.no_kpp)+kpp.noKpp
                tv_notifikasi_kpp_kgb.text = resources.getString(R.string.notifikasi)+kpp.notifikasi
                setOnClickListener {
                    listener(kpp)
                }
            }
        }

    }

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(p0: CharSequence?): FilterResults {
                val kppSearch = p0.toString()
                kppFilterList = if (kppSearch.isEmpty()){
                    listKpp
                }else{
                    val resultList = ArrayList<KPPItem>()
                    for (row in listKpp){
                        if (row.namaLengkap.toLowerCase(Locale.ROOT).contains(kppSearch.toLowerCase(Locale.ROOT))) {
                            resultList.add(row)
                        }
                    }
                    resultList
                }
                val filterResults = FilterResults()
                filterResults.values = kppFilterList
                return filterResults
            }

            override fun publishResults(p0: CharSequence?, p1: FilterResults?) {
                kppFilterList = p1?.values as ArrayList<KPPItem>
                notifyDataSetChanged()
            }

        }
    }

}