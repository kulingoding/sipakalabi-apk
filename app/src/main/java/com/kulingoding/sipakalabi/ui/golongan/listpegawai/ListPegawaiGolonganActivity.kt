package com.kulingoding.sipakalabi.ui.golongan.listpegawai

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.kulingoding.sipakalabi.R
import kotlinx.android.synthetic.main.activity_list_pegawai_golongan.*
import kotlinx.android.synthetic.main.activity_list_pegawai_golongan.toolbar
import org.koin.android.viewmodel.ext.android.viewModel

class ListPegawaiGolonganActivity : AppCompatActivity() {

    companion object{
        const val EXTRA_GOLONGAN = "extra_golongan"
    }

    private val viewModel: ListPegawaiGolonganViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list_pegawai_golongan)

        progress_bar_pegawai_golongan.visibility = View.VISIBLE

        val extras = intent.extras
        if (extras != null){
            val golongan = extras.getString(EXTRA_GOLONGAN).toString()

            setSupportActionBar(toolbar)
            supportActionBar?.title = "Data Pegawai $golongan"
            supportActionBar?.setDisplayHomeAsUpEnabled(true)


            viewModel.setGolongan(golongan)
            val adapterPegawai = ListPegawaiGolonganAdapter()
            viewModel.getListPegawaiGolonagn()?.observe(this, Observer {
                if (it.isEmpty()) tv_data_not_found.visibility = View.VISIBLE
                progress_bar_pegawai_golongan.visibility = View.GONE
                adapterPegawai.setPegawai(it)
                adapterPegawai.notifyDataSetChanged()
            })

            with(rv_pegawai_golongan){
                layoutManager = LinearLayoutManager(this@ListPegawaiGolonganActivity)
                setHasFixedSize(true)
                adapter = adapterPegawai
            }
        }

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean =
        when (item.itemId){
            android.R.id.home -> {
                this.finish()
                true
            }

            else -> {
                super.onOptionsItemSelected(item)
            }
        }
}