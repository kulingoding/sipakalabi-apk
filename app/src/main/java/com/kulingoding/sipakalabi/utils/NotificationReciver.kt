package com.kulingoding.sipakalabi.utils

import android.app.AlarmManager
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.media.RingtoneManager
import android.os.Build
import android.widget.Toast
import androidx.core.app.NotificationCompat
import androidx.lifecycle.LiveData
import com.kulingoding.sipakalabi.R
import com.kulingoding.sipakalabi.data.DataRepository
import com.kulingoding.sipakalabi.data.source.remote.response.KGB
import com.kulingoding.sipakalabi.data.source.remote.response.KgbResponse
import com.kulingoding.sipakalabi.data.source.remote.response.KppResponse
import com.kulingoding.sipakalabi.ui.home.notification.kpp.KppViewModel
import com.kulingoding.sipakalabi.ui.login.LoginActivity
import com.kulingoding.sipakalabi.ui.splash.SplashActivity
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

class NotificationReciver(private val dataRepository: DataRepository) : BroadcastReceiver() {

    companion object{
        const val TYPE_REPEATING = "RepeatingAlarm"
        const val EXTRA_MESSAGE = "message"
        const val EXTRA_TYPE = "type"

        const val ID_REPEATING = 101
        private const val TIME_FORMAT = "HH:mm"
    }

    private lateinit var nip : String
    private lateinit var jabatan : String
    private lateinit var unit : String

    override fun onReceive(context: Context, intent: Intent) {

        getKgb()
        getKpp()

    }

    fun setNip(nip: String){
        this.nip = nip
    }

    fun setJabatan(jabatan: String){
        this.jabatan = jabatan
    }

    fun setUnit(unit: String){
        this.unit = unit
    }

    fun getNipLocal(): LiveData<String>? = dataRepository.getNip()

    fun getJabatanLocal(): LiveData<String>? = dataRepository.getJabatan()

    fun getUnit(): LiveData<String>? = dataRepository.getUnit()

    fun getKgb(): LiveData<KgbResponse>? = dataRepository.getKgbNotif(nip, jabatan, unit)

    fun getKpp(): LiveData<KppResponse>? = dataRepository.getKppNotif(nip, jabatan, unit)

    fun showNotification(
        context: Context,
        title: String,
        message: String?,
        notifId: Int
    ) {

        val CHANNEL_ID = "Channel_id"
        val CHANNEL_NAME = "AlarmManager channel"

        val intent = Intent(context, SplashActivity::class.java)
        val pendingIntent = PendingIntent.getActivity(context, 0, intent, 0)

        val notificationManagerCompat = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        val alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        val builder = NotificationCompat.Builder(context, CHANNEL_ID)
            .setContentIntent(pendingIntent)
            .setSmallIcon(R.drawable.sulbar_logo)
            .setLargeIcon(BitmapFactory.decodeResource(context.resources, R.drawable.sulbar_logo))
            .setContentTitle(title)
            .setContentText(message)
            .setAutoCancel(true)
            .setSound(alarmSound)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(
                CHANNEL_ID,
                CHANNEL_NAME,
                NotificationManager.IMPORTANCE_DEFAULT)

            channel.description = CHANNEL_NAME
            builder.setChannelId(CHANNEL_ID)
            notificationManagerCompat.createNotificationChannel(channel)
        }

        val notification = builder.build()
        notificationManagerCompat.notify(1, notification)

    }

    fun setRepeatingNotifiaction(context: Context, type: String, time: String, message: String){

        if (isDateInvalid(time, TIME_FORMAT)) return

        val alarmManager = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager
        val intent = Intent(context, NotificationReciver::class.java)
        intent.putExtra(EXTRA_MESSAGE, message)
        intent.putExtra(EXTRA_TYPE, type)

        val calendar = Calendar.getInstance()
        calendar.set(Calendar.HOUR_OF_DAY, 9)
        calendar.set(Calendar.MINUTE, 0)
        calendar.set(Calendar.SECOND, 0)

        val pendingIntent = PendingIntent.getBroadcast(context, ID_REPEATING, intent, 0)
        alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP, calendar.timeInMillis, AlarmManager.INTERVAL_DAY, pendingIntent)

    }

    fun cancelNotification(context: Context, type: String){
        val alarmManager = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager
        val intent = Intent(context, NotificationReciver::class.java)
        val requestCode = ID_REPEATING
        val pendingIntent = PendingIntent.getBroadcast(context, requestCode, intent, 0)
        pendingIntent.cancel()

        alarmManager.cancel(pendingIntent)

    }

    private fun isDateInvalid(time: String, timeFormat: String): Boolean {
        return try {
            val df = SimpleDateFormat(timeFormat, Locale.getDefault())
            df.isLenient = false
            df.parse(time)
            false
        }catch (e: ParseException){
            true
        }
    }
}