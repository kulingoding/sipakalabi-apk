package com.kulingoding.sipakalabi.utils

import android.annotation.SuppressLint
import android.content.Context
import com.kulingoding.sipakalabi.R
import com.kulingoding.sipakalabi.data.source.local.entity.DashboardEntity

object DataLocal {

    @SuppressLint("UseCompatLoadingForDrawables")
    fun getMenuDashboardAdmin(context: Context): List<DashboardEntity>{

        val menu = ArrayList<DashboardEntity>()

        menu.add(
            DashboardEntity(
                "1",
                "Data Pegawai",
                context.getDrawable(R.drawable.ic_pegawai)
            ))

        menu.add(
            DashboardEntity(
                "2",
                "Data Golongan",
                context.getDrawable(R.drawable.ic_golongan)
            ))

        menu.add(
            DashboardEntity(
                "3",
                "Data SPPD",
                context.getDrawable(R.drawable.ic_koper)
            ))

        menu.add(
            DashboardEntity(
                "4",
                "Data PIP",
                context.getDrawable(R.drawable.ic_medal)
            ))

        menu.add(
            DashboardEntity(
                "5",
                "Keluar",
                context.getDrawable(R.drawable.ic_signout)
            ))

        return menu
    }

    @SuppressLint("UseCompatLoadingForDrawables")
    fun getMenuDashboardPegawai(context: Context): List<DashboardEntity>{

        val menu = ArrayList<DashboardEntity>()

        menu.add(
            DashboardEntity(
                "1",
                "Data Pegawai",
                context.getDrawable(R.drawable.ic_pegawai)
            ))

        menu.add(
            DashboardEntity(
                "3",
                "Data SPPD",
                context.getDrawable(R.drawable.ic_koper)
            ))

        menu.add(
            DashboardEntity(
                "4",
                "Data PIP",
                context.getDrawable(R.drawable.ic_medal)
            ))

        menu.add(
            DashboardEntity(
                "5",
                "Keluar",
                context.getDrawable(R.drawable.ic_signout)
            ))

        return menu
    }

    @SuppressLint("UseCompatLoadingForDrawables")
    fun getMenuDashboardPlus(context: Context): List<DashboardEntity>{

        val menu = ArrayList<DashboardEntity>()

        menu.add(
            DashboardEntity(
                "1",
                "Data Pegawai",
                context.getDrawable(R.drawable.ic_pegawai)
            ))

        menu.add(
            DashboardEntity(
                "2",
                "Data Golongan",
                context.getDrawable(R.drawable.ic_golongan)
            ))

        menu.add(
            DashboardEntity(
                "3",
                "Data SPPD",
                context.getDrawable(R.drawable.ic_koper)
            ))

        menu.add(
            DashboardEntity(
                "6",
                "Data SPPD Kadis",
                context.getDrawable(R.drawable.ic_travel)
            ))

        menu.add(
            DashboardEntity(
                "4",
                "Data PIP",
                context.getDrawable(R.drawable.ic_medal)
            ))

        menu.add(
            DashboardEntity(
                "5",
                "Keluar",
                context.getDrawable(R.drawable.ic_signout)
            ))

        return menu
    }
}