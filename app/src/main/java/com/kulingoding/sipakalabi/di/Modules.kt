package com.kulingoding.sipakalabi.di

import com.kulingoding.sipakalabi.data.DataRepository
import com.kulingoding.sipakalabi.data.source.local.PreferenceProvider
import com.kulingoding.sipakalabi.data.source.remote.RemoteDatasource
import com.kulingoding.sipakalabi.ui.golongan.GolonganViewModel
import com.kulingoding.sipakalabi.ui.golongan.listpegawai.ListPegawaiGolonganViewModel
import com.kulingoding.sipakalabi.ui.home.dashboard.DashboardViewModel
import com.kulingoding.sipakalabi.ui.home.notification.kgb.KgbViewModel
import com.kulingoding.sipakalabi.ui.home.notification.kpp.KppViewModel
import com.kulingoding.sipakalabi.ui.login.LoginViewModel
import com.kulingoding.sipakalabi.ui.pegawai.PegawaiViewModel
import com.kulingoding.sipakalabi.ui.pegawai.search.SearchPegawaiViewModel
import com.kulingoding.sipakalabi.ui.pip.PipViewModel
import com.kulingoding.sipakalabi.ui.pip.detailpip.DetailPipViewModel
import com.kulingoding.sipakalabi.ui.pip.search.SearchPipViewModel
import com.kulingoding.sipakalabi.ui.sppd.SppdViewModel
import com.kulingoding.sipakalabi.ui.sppd.search.SearchSppdViewModel
import com.kulingoding.sipakalabi.ui.sppdopd.SppdOpdViewModel
import com.kulingoding.sipakalabi.ui.sppdopd.search.SearchSppdOpdViewModel
import com.kulingoding.sipakalabi.utils.NotificationReciver
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

object Modules {
    val appModule = module{

        single { DataRepository(get(), get()) }
        single { RemoteDatasource() }
        single { PreferenceProvider(get()) }
        single { NotificationReciver(get()) }

        viewModel { LoginViewModel(get()) }
        viewModel { DashboardViewModel(get()) }
        viewModel { PegawaiViewModel(get()) }
        viewModel { KppViewModel(get()) }
        viewModel { KgbViewModel(get()) }
        viewModel { SppdViewModel(get()) }
        viewModel { GolonganViewModel(get()) }
        viewModel { PipViewModel(get()) }
        viewModel { SearchPegawaiViewModel(get()) }
        viewModel { SearchSppdViewModel(get()) }
        viewModel { ListPegawaiGolonganViewModel(get()) }
        viewModel { SearchPipViewModel(get()) }
        viewModel { SppdOpdViewModel(get()) }
        viewModel { SearchSppdOpdViewModel(get()) }
        viewModel { DetailPipViewModel(get()) }

    }
}